<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="jquery.wallform.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

        $('#photoimg').on('change', function() {
        
            var A = $("#imageloadstatus");
            var B = $("#imageloadbutton");

            $("#imageform").ajaxForm({
                target: '#preview',
                beforeSubmit: function() {
                    A.show();
                    B.hide();
                },
                success: function() {
                    A.hide();
                    B.show();
                },
                error: function() {
                    A.hide();
                    B.show();
                }
            });
        });
    });
</script>


