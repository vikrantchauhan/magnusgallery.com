<div id="attributepop" class="attr-container">

    <h3>Attribute Management<a href="<? echo base_url(); ?>mainpage/attribute"><img src="<? echo base_url(); ?>images/close7.png" class="closeicon" id="closeattributepop" /></a></h3>

	<form id="attrForm" method="post" action="<? echo base_url(); ?>update/attribute/<? echo $data[0]['id']; ?>">
		<input class="inputattribute" placeholder="Name" name="attribute-name" required value="<? echo $data[0]['name']; ?>">
		
		<div class="optionBox">
		
			<?
				
				$sub = $data[0]['sub'];
				
				if($sub == null){
			?>		
					<div class="block">
		            	<input type="text" class="inputblock" name="sub1"/> <span class="remove label"></span>
		            	<span class="addattributes label" value="1" ><a href="#" id="addattr"><img src="<? echo base_url(); ?>images/plus.png">Add Attribute</a></span>
		    		</div>
			<?
				}
				else{
					$sub = json_decode($sub, true);
					
					for($i = 0; $i < count($sub); $i++){
						
						$x = $i+1;
						if($x != count($sub)){
			?>
							<div class="block">
								<input type="text" class="inputblock" name="sub<? echo $x; ?>" value ="<? echo $sub[$i]; ?>"/>
								<span class="remove label"><a href="#"><img src="<? echo base_url(); ?>images/delete.png">Remove</a></span>
							</div>
			<?
						}
						else{
			?>				
							<div class="block">
						    	<input type="text" class="inputblock" name="sub<? echo $x; ?>" value ="<? echo $sub[$i]; ?>" /> <span class="remove label"></span>
						    	<span class="addattributes label" value="<? echo $x; ?>" ><a href="#" id="addattr"><img src="<? echo base_url(); ?>images/plus.png">Add Attribute</a></span>
							</div>
			<?			}
					}
				}
			?>
			
		   
		</div>
		
		<button type="submit" id="save">Save</button>
	</form>

    <br/>
    <br/>

</div>
</div>
