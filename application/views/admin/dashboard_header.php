<header>
    <div class="logo">
    	<em>Magnus</em>
    </div>
    
    <!--div class="search">
        <input placeholder="Search">
        <img src="../images/search.png">
    </div-->
    
    <div class="admin">
        <a href="logout">
        	Logout
        </a>Admin
	</div>
</header>


<div class="main">

    <div class="side"><!---------------side div------------------->

		<a href="/mainpage/dashboard" > 
			<div class="<?php print(($menu['main']=='dashboard' ? 'activeMain' : 'menu')); ?>"  id="menu1">
				<img src="<? echo base_url(); ?>images/dashboard-icon.png" />Dashboard
			</div>
		</a>


		
		<div class="<?php print(($menu['main']=='product' ? 'activeMain' : 'menu')); ?>" id="menu2">
			<img src="<? echo base_url(); ?>images/catalog_small.png" />Catalog Management &nbsp &nbsp &nbsp &nbsp<img src="<? echo base_url(); ?>images/arrow.png" />
		</div>
		

		<div class="<?php print(($menu['main']=='product' ? 'procatActive' : 'procat')); ?>" id="procat">

			<a href="/mainpage/catalog">
				<div class="<?php print(($menu['sub']=='catalog' ? 'proActive' : 'pro')); ?>" id="productmenu">Catalog</div>
			</a>
	
			<a href="/mainpage/category">
				<div class="<?php print(($menu['sub']=='category' ? 'proActive' : 'pro')); ?>" id="categorymenu">Category</div>
			</a>
			
			<a href="/mainpage/attribute">
				<div class="<?php print(($menu['sub']=='attribute' ? 'proActive' : 'pro')); ?>" id="attributes">Attribute</div>
			</a>
		</div>

    	<div class="<?php print(($menu['main']=='web' ? 'activeMain' : 'menu')); ?>" id="menu3"> 
    		<img src="<? echo base_url(); ?>images/tool.png" />Website Management &nbsp &nbsp &nbsp <img src="<? echo base_url(); ?>images/arrow.png" />
    	</div>

        <div class="<?php print(($menu['main']=='web' ? 'procatActive' : 'procat')); ?>" id="staticmenu">

            <a href="/mainpage/menu">
            	<div class="<?php print(($menu['sub']=='menu' ? 'proActive' : 'pro')); ?>" id="smenu">Menu</div>
            </a>
        </div>
	</div>
	<!---------------side div closed------------------->
