<div id="addpop" class="pro-container">
	
	<h3>Add Product
		<a href="<? echo base_url(); ?>mainpage/catalog">
			<img src="<? echo base_url(); ?>images/close7.png" class="closeicon" id="closeaddpop" />
		</a>
	</h3>
	
	<div class="headmenu">
		
		<div class="tabs">
		    
		    <div class="tab">
		       
		        <input type="radio" id="tab-1" name="tab-group-1" >
		        <label for="tab-1">Basic</label>
		        
		        <div class="content">
		        	
		        	<form method="post" action="<? echo base_url(); ?>catalog/update/<? echo $pId; ?>" id="productForm">
					
						<div class="row">
							<input type="text" class="input" name="name" id="product-name" value="<? echo $info['name']; ?>" required placeholder="Product Name">
							<textarea class="input" name="description" id="product-desc" required placeholder="Product Description"><? $desc = str_replace("<br />", "", $info['description']); echo $desc; ?></textarea>
							<select class="input" name="category" id="attr-category">
							
								<?php
									for($i = 0; $i < count($category); $i++){
										
										$item = $category[$i];
								?>
								
									<option value="<?php echo $item['id']; ?>" <? print(($item['id'] == $categoryId) ? "selected" : '')?>><?php echo $item['name']; ?></option>	
								<?	
									}
								?>
								
							</select>
						</div>
						
						
						<div class="row-attr row" >	
									
						<?
							
							
							for($i = 0; $i < count($attrs); $i++){
								
								$item = $attrs[$i];
								
								if($item['sub'] == null){
						?>
						
									<textarea type="text" style="height: 35px;" class="input" name="<?php print($item['id'].'-'.$item['name']); ?>" id="product-name" placeholder="<?php echo $item['name'] ?>"><? 
									$text = (array_key_exists($item['name'], $addedAttrs)) ? $addedAttrs[$item['name']] : '';
									$text = str_replace("<br />", "", $text);
									print_r($text);	
									?></textarea>
						<?	
								}
								else{
								
									$subVal = "";
									$subArray = array();
									
									if(array_key_exists($item['name'], $addedAttrs)){
									
										$subVal = $addedAttrs[$item['name']];
										
										$subVal = explode("<br>", $subVal);
																				
										for($x = 0; $x < count($subVal); $x++){
											
											$subItem = $subVal[$x];
											
											if($subItem != null){
												$subItem = explode(' ', $subItem);
												$subArray[$subItem[0]] = $subItem[1];
											}
										}
									}
						?>
									<span><? echo $item['name']; ?></span>
						<?			
									$sub = json_decode($item['sub'], true);
										
									for($x = 0; $x < count($sub); $x++){
						?>					
										<input type="text" class="sub-input" name="<?php print($item['id'].'-'.$item['name'].'-'.$sub[$x]); ?>" id="product-name" placeholder="<?php echo $sub[$x] ?>" value="<? print((array_key_exists($sub[$x], $subArray)) ? $subArray[$sub[$x]] : '');?>">
						<?			}
								}
							}
						?>
						</div>
					
						
						<input type="submit" value="Update Product" class="sub_edit">
            		</form>
            	</div>
        	</div><!------------------------tab1 div ended------------------------------------>
			
			<div class="tab">
				
				<input type="radio" id="tab-2" name="tab-group-1" checked>
				<label for="tab-2">Images</label>
			
				<div class="content">
					<form id="imageform" method="post" enctype="multipart/form-data" action="<? echo base_url()?>catalog/uploadPhoto/<? echo $pId; ?>" style="clear:both">
						Upload image: 
						<div id='imageloadstatus' style='display:none'><img src="loader.gif" alt="Uploading...."/></div>
	
						<div id='imageloadbutton'>
							<input type="file" name="photos[]" id="photoimg" multiple="true" />
						</div>
					</form>
					<div id="preview" class="img-preview">
					<?
						if(array_key_exists('images', $info) && $info['images'] != null){
						
							$images = json_decode($info['images'], true);
							
							for($i = 0; $i < count($images); $i++){
								$img = $images[$i];
					?>
								<div class="preview-box">
									<img src="<? echo base_url(); ?>upload/<? echo $img; ?>" class="imgList">
									<a href="<? echo base_url(); ?>catalog/removeImage/<? echo $img.'/'.$pId; ?>" class="img-remove">
										<img src="<? echo base_url(); ?>images/close7.png">
									</a>
								</div>			
					<?
					
							}
						}
					?>
					</div>
					
					<!--input type="file" id="choose" multiple="multiple" class="custom-file-input tabinput" />
					<br>
					<div class="uploadPreview"></div-------->
				</div> 
			</div><!------------------------tab2 div ended------------------------------------>
			
			<div class="tab">
			
				<input type="radio" id="tab-3" name="tab-group-1">
				<label for="tab-3">360 &deg Images</label>
				
				<div class="content">
					<form id="imagedeg" method="post" enctype="multipart/form-data" action="<? echo base_url()?>catalog/uploadDegImage/<? echo $pId; ?>" style="clear:both">
						<br>
						Upload image: 
						<div id='loadstatus' style='display:none'><img src="loader.gif" alt="Uploading...."/></div>
	
						<div id='loadbutton'>
							
							<input type="file" name="photos[]" id="photodeg" multiple="true" />
						</div>
					</form>

					<div id='degpreview' class="img-preview">
					<?
						if(array_key_exists('panorama', $info) && $info['panorama'] != null){
						
							$images = json_decode($info['panorama'], true);
							
							for($i = 0; $i < count($images); $i++){
								$img = $images[$i];
					?>
								<div class="preview-box">
									<img src="<? echo base_url(); ?>upload/<? echo $pId.'/'.$img; ?>" class="imgList">
									<a href="<? echo base_url(); ?>catalog/removeDegImage/<? echo $img.'/'.$pId; ?>" class="img-remove">
										<img src="<? echo base_url(); ?>images/close7.png">
									</a>
								</div>		
					<?
					
							}
						}
					?>
					</div>
				</div>
			</div><!------------------------tab3 div ended------------------------------------>
        
    	</div><!------------------------tabs div ended------------------------------------>
	</div><!------------------------headmenu div ended------------------------------------>
</div>
</div> <!----------------------------------main div ended------------------------------------->

