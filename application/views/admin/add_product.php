
<div id="addpop" class="pro-container">
	
	<h3>Add Product
		<a href="<? echo base_url(); ?>mainpage/catalog">
			<img src="<? echo base_url(); ?>images/close7.png" class="closeicon" id="closeaddpop" />
		</a>
	</h3>
	
	<div class="headmenu">
		
		<div class="tabs">
		    
		    <div class="tab">
		       
		        <input type="radio" id="tab-1" name="tab-group-1" checked >
		        <label for="tab-1">Basic</label>
		        
		        <div class="content">
		        	
		        	<form method="post" action="<? echo base_url(); ?>catalog/save" id="productForm">
					
						<div class="row">
							<input type="text" class="input" name="name" id="product-name" required placeholder="Product Name">
							<textarea class="input" name="description" id="product-desc" required placeholder="Product Description"></textarea>
							<select class="input" name="category" id="attr-category">
								<?php
									for($i = 0; $i < count($category); $i++){
										
										$item = $category[$i];
								?>
								
									<option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>	
								<?	
									}
								?>
							
							</select>
						</div>
					
						<div class="row-attr row" >	
									
						<?	
							for($i = 0; $i < count($attrs); $i++){
								
								$item = $attrs[$i];
								
								if($item['sub'] == null){
						?>
						
									<textarea type="text" style="height: 35px;" class="input" name="<?php print($item['id'].'-'.$item['name']); ?>" id="product-name" placeholder="<?php echo $item['name'] ?>"></textarea>
						<?	
								}
								else{
						?>
									<span><? echo $item['name']; ?></span>
						<?			
									$sub = json_decode($item['sub'], true);
										
									for($x = 0; $x < count($sub); $x++){
						?>					
										<input type="text" class="sub-input" name="<?php print($item['id'].'-'.$item['name'].'-'.$sub[$x]); ?>" id="product-name" placeholder="<?php echo $sub[$x] ?>">
						<?			}
								}
							}
						?>
						</div>
						
						
						<input type="submit" value="Add Product" class="sub_edit">
            		</form>
            	</div>
        	</div><!------------------------tab1 div ended------------------------------------>
			
			<div class="tab">
				
				<input type="radio" id="tab-2" name="tab-group-1" disabled>
				<label for="tab-2">Images</label>
			
				<div class="content">
					<form id="imageform" method="post" enctype="multipart/form-data" action="uploadPhoto/1" style="clear:both">
						Upload image: 
						<div id='imageloadstatus' style='display:none'><img src="loader.gif" alt="Uploading...."/></div>
	
						<div id='imageloadbutton'>
							<input type="file" name="photos[]" id="photoimg" multiple="true" />

						</div>
					</form>

					<div id='preview'></div>
					<!--input type="file" id="choose" multiple="multiple" class="custom-file-input tabinput" />
					<br>
					<div class="uploadPreview"></div-------->
				</div> 
			</div><!------------------------tab2 div ended------------------------------------>
			
			<div class="tab">
			
				<input type="radio" id="tab-3" name="tab-group-1" disabled>
				<label for="tab-3">360 &deg Images</label>
				
				<div class="content">
					<form id="imagedeg" method="post" enctype="multipart/form-data" action="<? echo base_url()?>catalog/uploadDegImage/<? echo $pId; ?>" style="clear:both">
						Upload image: 
						<div id='loadstatus' style='display:none'><img src="loader.gif" alt="Uploading...."/></div>
	
						<div id='loadbutton'>
							<input type="file" name="photos[]" id="photodeg" multiple="true" />

						</div>
					</form>

					<div id='degpreview'></div>
				</div>
			</div><!------------------------tab3 div ended------------------------------------>
        
    	</div><!------------------------tabs div ended------------------------------------>
	</div><!------------------------headmenu div ended------------------------------------>
	
</div>
</div> <!----------------------------------main div ended------------------------------------->



