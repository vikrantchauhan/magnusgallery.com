<!------------------------maincontainer2 Div Started------------------------------------------>
<div class="maincontainer2" id="container2">

    <div class="products">Products</div>

    <a href="<? echo base_url(); ?>catalog/add">
    	<div class="addproduct" id="addproduct">+Add Product</div>
    </a>

	<?php 
		
		for($i = 0; $i < count($list); $i++){
			
			$item = $list[$i];
			$images = json_decode($item['images'], true);
	?>
		<div class="productitems">
			<img src="<? echo base_url(); ?>upload/<?php echo $images[0]; ?>" class="productimage" />
			
			<div class="aboutproduct">
				<h4><?php echo $item['name']; ?> </h4>
				<h6><?php echo $item['category']; ?>  </h6>
			</div>		
			
			<a href="deletePro/<? echo $item['id']; ?>">
				<img src="<? echo base_url(); ?>images/close.png" class="closeicon" />
			</a>
			
			<div class="editview1">

				<div class="edit">
					<a href="<? echo base_url(); ?>catalog/edit/<? echo $item['id']; ?>">Edit</a>&nbsp&nbsp&nbsp<img src="<? echo base_url(); ?>images/edit.png" />
				</div>
			</div>
		</div>
	<?php	
		}	
	?>
	
</div>
<!------------------------maincontainer2 Div Ended------------------------------------------>

</div> <!----------------------------------main div ended------------------------------------->
