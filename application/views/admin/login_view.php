 <header>
    <div class="logo"><em>Magnus</em></div>
</header>
        
<div id="login" class="logbox">

    <div class="logtop">Login to your Account</div>
    
    <div class="form">
    
        <form role="form" id="login-form" action="<?php echo base_url();?>admin/isAdmin" method="POST">
            
            <input type="text" id="user-input" name="username" placeholder="Username"class="input" type="text" autofocus required ><br/>
            
            <input type="password" id="pwd-input" name="password" class="input" placeholder="Password" required><br/>
            <button type="submit" class="loginsubmit">Login</button><br/>
            
            <!--div class="lastlog">
                <input class="remember" type="checkbox" name="remember" value="Remember">
                <span>Remember me</span>
                <a href="#"><span class="forgot">Forgot Password ?</span></a>
            </div>
            <br/><br/>
            <div class="line">
                <hr>
            </div>
            <div class="linetext">Or </div>
            <div class="line">
                <hr>
            </div-->
            <div id="login-error"><?php if($error) {echo $error;} ?></div>
        </form>
        
        <!--a href="#">
        	<img src="<?php echo base_url(); ?>images/facebook-logo.png"/>
        </a>
        <a href="#">
        	<img src="<?php echo base_url(); ?>images/google-plus-logo.png"/>
        </a-->
    </div>
</div>

