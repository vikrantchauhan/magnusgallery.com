<!DOCTYPE html>
<html>

<head>
    <title>
        Magnus Homepage
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes" />

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.flexisel.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main_site.css">
</head>

<body>
    <div class="menu-box">
        <div class="headermenu">
            <div>
                <a href="#"><img src="../img/menu-icon.png" onclick="clicker()">
                    <span style="font-family: tw-cen-mt REGULAR" onclick="clicker()"> Menu </span>
                </a>
            </div>
            <div style="text-align: center; font-family: tw-cen-mt REGULAR; font-size: 1.2em"> MAGNUS </div>
            <div style="text-align: right;">
                <a href="#"><img src="../img/facebook.png">
                </a>
                <a href="#"><img id="tweet" src="../img/twitter.png">
                </a>
            </div>
        </div>

		<div id="displaybox">
			
			<a class="page-scroll" href="#close" onclick="start()"> 
				<img src="../img/cancel30.png" />
			</a>
			
			<?php 
				for($i = 0; $i < count($menu); $i++){
				
					$item = $menu[$i];
					
					if(empty($item['sub'])){
			?>		
					
					<a class="page-scroll box" href="#<?php echo strtolower(str_replace(' ', '',$item['name'])); ?>" onclick="start()"><?php echo $item['name']?></a>
			
			<?php	
					}
					else{
			?>
						<a class="page-scroll box" onclick="start()"><?php echo $item['name']?>  <span style="font-size: 0.5em">+ </span></a>
			<?php
						for($x = 0; $x < count($item['sub']); $x++){
							
							$sub = $item['sub'][$x];
			?>
							<div class="d">
								<a href="<?php echo base_url(); ?><?php echo str_replace(' ', '-', $sub['name']); ?>"> <?php echo $sub['name']; ?> </a>
							</div>
			<?php
						}
					}
				}
			?>

		</div>

