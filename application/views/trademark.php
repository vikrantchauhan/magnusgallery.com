<!DOCTYPE html>
<html>

<head>
    <title>
        Magnus Trademarks
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main_site.css">
	<link rel="stylesheet" type="text/css" href="css/trademark.css">
</head>

<body>
      <header>
          <img src="img/menu-icon.png" id="menu" onclick="clicker()"/><span onclick="clicker()">Menu</span>
        <div class="hnote">
             MAGNUS ART COLLECTIONS LLC.
        </div>
    </header>
<div id="displaybox">
		
		<a class="page-scroll" href="#close" onclick="start()"> 
			<img src="../img/cancel30.png" />
		</a>
		
		<?php 
			for($i = 0; $i < count($menu); $i++){
			
				$item = $menu[$i];
				
				if(empty($item['sub'])){
		?>		
				
				<a class="page-scroll box" href="<? echo base_url()?>#<?php echo strtolower(str_replace(' ', '',$item['name'])); ?>" onclick="start()"><?php echo $item['name']?></a>
		
		<?php	
				}
				else{
		?>
					<a class="page-scroll box" href="#" onclick="start()"><?php echo $item['name']?>  <span style="font-size: 0.5em">+ </span></a>
		<?php
					for($x = 0; $x < count($item['sub']); $x++){
						
						$sub = $item['sub'][$x];
		?>
						<div class="d">
							<a href="<?php echo base_url(); ?><?php echo str_replace(' ', '-', $sub['name']); ?>"> <?php echo $sub['name']; ?> </a>
						</div>
		<?php
					}
				}
			}
		?>

	</div>

    <h4 class="trade-head">
        Trademark Disclaimer
    </h4>
    <p class="trade">
        Product names, logos, brands, and other trademarks featured or referred to within MAGNUS ART COLLECTIONS, 
        LLC products and services and within MAGNUSGALLERY.com are the property of their respective trademark holders. 
        These trademark holders are not affiliated with MAGNUS ART COLLECTIONS, LLC, our products, or our website. 
        They do not sponsor or endorse our materials. Here is a partial listing of these trademarks and their owners.
        This list is subject to change without notice.
    </p>

	<table>
        <tr>
            <th class="tr">Trademark</th>
            <th class="own">Owner</th> 
        </tr>
        <tr>
            <td>
                HARLEY DAVIDSON
            </td>
	    <td>
                HARLEY DAVIDSON is a registered trademark of H-D USA, LLC. Magnus Art Collections is not affiliated with the trademark owner.
            </td>
        </tr>
        
        <tr>
            <td>
                FORD
            </td>
	    <td>
                FORD is a registered trademark of Ford Motor Company. Magnus Art Collections is not affiliated with the trademark owner.
            </td>
        </tr>
        
        <tr>
            <td>
                MODEL T
            </td>
	    <td>
            MODEL T is a registered trademark of Ford Motor Company. Magnus Art Collections is not affiliated with the trademark owner.            </td>
        </tr>
        
        <tr>
            <td>
                CADILLAC
            </td>
	    <td>
            CADILLAC is a registered trademark of General Motors, LLC.  Magnus Art Collections is not affiliated with the trademark owner.            </td>
        </tr>
        
        <tr>
            <td>
                OLDSMOBILE
            </td>
	    <td>
            OLDSMOBILE is a registered trademark of General Motors, LLC.  Magnus Art Collections is not affiliated with the trademark owner.            </td>
        </tr>
        
        <tr>
            <td>
                DUESENBERG
            </td>
	    <td>
            DUESENBERG is a registered trademark of Brightcliff Limited.  Magnus Art Collections is not affiliate with the trademark owner.            </td>
        </tr>
        
        
        
    </table>

<section class="last" id="contactus">
        <div class="h">
            <img src="../img/contact-us-icon.png" style="vertical-align: middle; padding: 1%; " />Contact<span class="g">us</span>
        </div>
        <div class="lastone">
            <div class="lastone1"><a href="#"> CONTACT@MAGNUSGALLERY.COM </a>
            </div>
            <div class="lastone1"><a href="#"> WWW.MAGNUSGALLERY.COM </a>
            </div>
        </div>
    </section>
     <div class="copyright">
        
        <p>COPYRIGHT &copy 2016 MAGNUS</p>
        
        <p><a href="#top" class="page-scroll"> TAKE ME BACK TO TOP </a>  </p>
    
    
        <p class="trademark">* Trademark Desclaimer &nbsp<a href="<?php echo base_url(); ?>trademark"><u>Click Here</u></a></p>
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
     <script type="text/javascript" src="js/Home.js"></script>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
  
    <!-- Plugin JavaScript -->
   <script src="js/jquery.easing.min.js"></script>

</body>
</html>
