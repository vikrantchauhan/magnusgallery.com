function start() {
    $("#signup").show();
    $("#login").hide();
}
function s1() {
    $("#signup").hide();
    $("#login").show();
}
function myFunction(){
    $("#signup").hide();
}

$(document).ready(function(){

	$('form#login-form').on('submit',  function(e) {
	
		e.preventDefault();
		
		var user = $('#user-input').val();
		var pass = $('#pwd-input').val();
		var obj = $(this),
			url = obj.attr('action'),
			method = obj.attr('method');
			
		$.ajax({
		
			url: url,
			type: method,
			data: {'username' : user, 'password': pass},
			success: function(response) {
				if(response !=  "not"){
					window.location.replace(base_url+"mainpage/dashboard");
				}
				else{
					$("#login-error").html("**Admin credentials are wrong!!");
				}
			}
		});
	});

});
