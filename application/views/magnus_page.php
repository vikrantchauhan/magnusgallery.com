<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main_site.css">

        <div class="topcenter" id="top">
            <div class="menuicon">
                <a href="#"><img src="../img/menu-icon.png" onclick="clicker()">
                    <span class="menutext" onclick="clicker()"> Menu </span>
                </a>
            </div>
            <br style="clear: left;">
            <img src="../img/banner-logo.png" class="centerlogo">
            <div class="centerhead">
                <p class="hding">
                    INSPIRED BY
                </p>
                <p class="subhding">
                    Passion
                </p>
                <p class="hding">
                    DISCOVER OUR
                </p>
                <p class="subhding">
                    Handcrafted Collections
                </p>
            </div>
        </div>
        <div id="sroll">
            <a href="#collection" class="page-scroll" style="color: #FFFFFF">SCROLL DOWN</a>

        </div>


    </div>


    <div class="content container" id="collection">
        <div class="content-header" >
            <p class="ch-first">Vintedge</p>
            <p class="ch-second">MOTORCYCLES & CARS </p>

        </div>
    </div>


    <!--#######################################################image slider  #######################################################################-->

    <section class="gallery_1">
            
            <!--div class="slider_1">

                <ul>
					<?php 
						
						for($i = 0; $i < count($inventory); $i++){
							
							$item = $inventory[$i];
							$images = json_decode($item['images'], true);
					?>
						<li>
				            <div class="showimage"><img src="<?php echo base_url()?>upload/<?php echo $images[0]; ?>" class="slides" alt="Chania">
				                <br/>
				                <div class="info"> <?php echo $item['name']; ?>	
					               <div> <a href="<?php echo base_url(); ?><?php echo str_replace(' ', '-',$item['name']) ?>">Details </a></div>
				                </div>
				            </div>
				        </li>
					<?php
						}
					?>
                </ul>

            </div>


				
			

            <div id="slider-nav_1" style="display: block!important;">
		        <div class="button" data-dir="prev_1"><img src="../img/left-arrow.png" class="prev"></div>
		        <div class="button" data-dir="next_1"><img src="../img/right-arrow.png" class="next"></div>
                
                
            </div-->
		<ul id="flexiselDemo3">
		
			<?php 
						
				for($i = 0; $i < count($inventory); $i++){
					
					$item = $inventory[$i];
					$images = json_decode($item['images'], true);
			?>
				<li>
		            <div class="showimage"><img src="<?php echo base_url()?>upload/<?php echo $images[0]; ?>" class="slides" alt="Chania">
		                <br/>
		                <div class="info"> <?php echo $item['name']; ?>	
			               <div> <a href="<?php echo base_url(); ?><?php echo str_replace(' ', '-',$item['name']) ?>">Details </a></div>
		                </div>
		            </div>
		        </li>
			<?php
				}
			?>                                              
		</ul> 

    </section>
   



    <!--##########################################################end of Slider#################################################################-->





    <section class="content-about" id="content-about">

        <div class="divide">
            <div class="about" id="about">

                <div class="ab">About
                    <br/><div class="m">MAGNUS</div>
                </div>

                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>


            </div>
        </div>
        <div class="divide">
            <div class="team">

                <div class="ab">Our
                    <br/><div class="m">TEAM</div>
                </div>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
        </div>
    </section>



    <section class="content-footer" id="content-footer">

        <div class="footer-fi" id="finecraftsmanship">FINE</div>
        <div class="footer-se">Craftsmanship</div>
    

        <div class="seperate">
            <div class="footerfade">
                <span class="fadetext fi">TRULY&nbsp;<span class="fadetext">VINTEDGE<br></span></span>
                <span class="fadetext se">HANDCRAFTED<br></span>
                <span class="fadetext th">TIMELESS<br></span>
                <span class="fadetext fr">CUSTOM&nbsp;<span class="fadetext">MADE<br></span></span>
                <span class="fadetext ff">STUNNING<br></span>
                <span class="fadetext si">FINE<br></span>
            </div>

        </div>


        <div class="seperate">

            <div class="footer-p">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </div>
        </div>
    </section>



    </div>
    <section class="last" id="contact">
        <div class="h">
            <a href="#"><img src="../img/contact-us-icon.png" style="vertical-align: middle; padding: 1%;" />Contact<span class="g">Us</span>
            </a>
        </div>
        <div class="lastone">
            <div class="lastone1"><a href="#"> CONTACT@MAGNUSGALLERY.COM </a>
            </div>
            <div class="lastone1"><a href="#"> WWW.MAGNUSGALLERY.COM </a>
            </div>
        </div>
    </section>
    <div class="copyright">
        <p>COPYRIGHT &copy 2016 MAGNUS &nbsp &nbsp &nbsp<a href="#top" class="page-scroll"> TAKE ME BACK TO TOP </a>
        </p>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/Home.js"></script>
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>js/jquery.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>js/jquery.easing.min.js"></script>
	




    <script type="text/javascript">
        $(document).scroll(function() {
            var y = $(this).scrollTop();
            if (y > 400) {
                $('.headermenu').fadeIn();

            } else {
                $('.headermenu').fadeOut();

            }
        });
       

    </script>
    <script type="text/javascript">
        $(window).on('beforeunload', function() {
            $(window).scrollTop(0);
        });
    </script>

	<script type="text/javascript">

		$(window).load(function() {

			$("#flexiselDemo3").flexisel({
				visibleItems: 4,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
				    portrait: { 
				        changePoint:480,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:640,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:768,
				        visibleItems: 3
				    }
				}
			});
		});
	</script>
    <!--script type="text/javascript">
            // 
            jQuery(document).ready(function ($) {
                
                // creating a container variable to hold the 'UL' elements. It uses method chaining.
                var container_1=$('div.slider_1')
                                            .css('overflow','hidden')
                                            .children('ul');
              
               
                // Creating the 'slider' instance which will set initial parameters for the Slider.
                var sliderobj_1= new slider(container_1,$('#slider-nav_1'));
             
                /*
                This will trigger the 'setCurrentPos' and 'transition' methods on click of any button
                "data-dir" attribute associated with the button will determine the direction of sliding.
                */
                sliderobj_1.nav.find('.button').on('click', function(){
                    sliderobj_1.setCurrentPos($(this).data('dir'));
                    sliderobj_1.transition();
                });
            });
    </script-->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>js/creative.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.flexisel.js"></script>
</body>

</html>
