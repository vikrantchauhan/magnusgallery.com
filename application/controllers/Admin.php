<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __Construct(){
        parent::__Construct();
        $this->load->model('main_model');
        $this->load->helper(array('form', 'url'));
    }

	function index(){
		$data['error'] = null;
		$this->load->view('admin/header');
		$this->load->view('admin/login_view', $data);
		$this->load->view('admin/footer');
	}
	
	
	function check(){
		$hash = base64_encode(hash_hmac('sha256', $password, "vintage"));
		echo $hash;
	}
		
	/**description* Check if the user is admin or not and if is admin then load 
	  * admin view 
 	  * @params String (Username)
 	  * @params String (Password)
 	  * @return Boolean (true/false)
	  */
	public function isAdmin(){
	
		$user = $this->input->post("username");
		$password = $this->input->post("password");
		
		$hash = base64_encode(hash_hmac('sha256', $password, "vintage"));
		
		$res = $this->main_model->isAdmin($user,$hash);

		if(empty($res)){
			print_r("not");
		}
		else{
		    $this->setSession($res[0]['id'],$res[0]['name']);
			print_r(json_encode($res));
		}
	}
	
		
	/**@desc function to set the session variables in this function
 	  *@param customer id, email and account id
	  *@return set session
	  */
 	function setSession($userId,$userName) {
	    
    	$userSession = array('userId'=>$userId,
                         		'userName'=>$userName,
                         		'lastActivity' => $_SERVER["REQUEST_TIME"],
                         		'sess_expiration'=>3600,
                         		'loggedIn'=>TRUE );
		$this->session->set_userdata($userSession);
 	}
	
}
