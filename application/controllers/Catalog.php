<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {


	function __Construct(){
        parent::__Construct();
        $this->load->model('main_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('ajaximageuploader');
    }
    
        
    
    function add(){
    	   		
    	if($this->checkSession()){
    	
			$data["attrs"] = $this->main_model->getAttributeList();
			$data['category'] = $this->main_model->getCategoryList(); 
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"catalog");
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/add_product', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    function save(){
    
    	if($this->checkSession()){
    	
			$category = $this->input->post("category");
			$productArray = $this->createProductInfoArray($_POST, $category);
			
			$attrArray = $this->createAttrArray($_POST);
			$result = $this->main_model->addProduct($attrArray, $productArray, $category);
			
			if($result)
				$this->addImages($result, $attrArray, $productArray, $category);
    	}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
            
    function createProductInfoArray($data, $category){
    	
    	$productArray = array();
    	
    	foreach($data as $key=>$value){
				
			if($value != null){
				
				$key = str_replace($category, "", $key);
				$productArray[$key] = nl2br($value);
			}
		}
		
		return $productArray;
    }  
    
    function addImages($result, $attrArray, $productArray, $category){
    	
    	if($result){
    	
			$data["attrs"] = $this->main_model->getAttributeList();
			$data['addedAttrs'] = $this->getProductAttrs($result);;
			$data['info'] = $productArray;
			$data['category'] = $this->main_model->getCategoryList(); 
			$data['categoryId'] = $category;
			$data['pId'] = $result; 
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"catalog");
			
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/edit_product', $data);
			$this->load->view('admin/footer');
		}
    }
    
    
    function createAttrArray($data){
    
    	$attrs = $this->main_model->getAttributeList();
    
    	$attrArray = array();
    	
    	for($i = 0; $i < count($attrs); $i++){
				
			$name = $attrs[$i]['name'];
			$id = $attrs[$i]['id'];
			
			if($attrs[$i]['sub'] != null){
				
				$sub = json_decode($attrs[$i]['sub'], true);
				
				$substr =  "";
				
				for($x = 0; $x < count($sub); $x++){
				
					$subItem = $sub[$x];
					$subVal = $data[$id.'-'.$name.'-'.$subItem];
					
					if($subVal != null && $subVal != '')
						$substr .= $subItem." ".$subVal."<br>";
					unset($data[$id.'-'.$name.'-'.$subItem]);
				}
				
				$attrArray[$id] = $substr;
			}
			else{
			
				$val = $data[$id.'-'.$name];
				if($val != null && $val !='')
					$attrArray[$id] = nl2br($val);	
					
				unset($data[$id.'-'.$name]);
			}
		}
		return $attrArray;
    }
    
       
        
    /*function createProductAttrArray($data, $category){
    
    	$attrArray = array();
    	
    	for($i = 0; $i < count($attrs); $i++){
				
			$attr = $attrs[$i]['name'];
			
			if($attrs[$i]['sub'] != null){
				
				$sub = json_decode($attrs[$i]['sub'], true);
				
				$substr =  "";
				
				for($x = 0; $x < count($sub); $x++){
					
					$substr .= $sub[$x]." ".$data[$sub[$x].$category]."<br>";
					unset($data[$sub[$x].$category]);
				}
				
				$attrArray[$attr] = $substr;
			}
			else{
				$attrArray[$attr] = nl2br($data[$attr.$category]);	
				unset($data[$attr.$category]);
			}
		}
		return $attrArray;
    }*/
    
    
    function getProductAttrs($id){
    	
    	$attr = $this->main_model->getProductAttrs($id);
    	$array = array();
    	
    	for($i = 0; $i < count($attr); $i++){
    		
    		$item = $attr[$i];
    		$array[$item['name']] = $item['value'];
    	}
    	return $array;
    }

            
    function edit($id){
    	
    	$info = $this->main_model->getProductInfo($id);
    	
    	$data["attrs"] = $this->main_model->getAttributeList();
		$data['addedAttrs'] = $this->getProductAttrs($id);
		$data['info'] = $info;
		$data['categoryId'] = $info['category'];
		$data['category'] = $this->main_model->getCategoryList(); 
		$data['pId'] = $id; 
		$data['error'] = null;
		$data['menu'] = array("main" => 'product', "sub"=>"catalog");
		
		$this->load->view('admin/header');
		$this->load->view('admin/dashboard_header', $data);
		$this->load->view('admin/edit_product', $data);
		$this->load->view('admin/footer');
    }
    
    
    
	/**description* Function to upload a photo to server and send request to 
	  * add/edit/duplicateapplication
	  * @params Array (Config params)
	  * @params String (Type of action to be performed)
	  * @params String (App id)
	  */
	function uploadPhoto($id){
	   		
    	if($this->checkSession()){
    	
			$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
			
			if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
		
				$uploaddir = "upload/"; //Image upload directory
				
				foreach ($_FILES['photos']['name'] as $name => $value){
			
					$filename = stripslashes($_FILES['photos']['name'][$name]);
					$size=filesize($_FILES['photos']['tmp_name'][$name]);

					$image_name=time().$filename; 
					echo '<div class="preview-box"><img src="'.base_url().$uploaddir.$image_name.'" class="imgList"><a href="'.base_url().'catalog/removeImage/'.$image_name.'/'.$id.'" class="img-remove"><img src="'.base_url().'/images/close7.png"></a></div>'; 
					$newname=$uploaddir.$image_name; 

					//Moving file to uploads folder
					if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname)) { 
				
						$this->addImagesToDb($image_name, $id);
					}
					else { 
						echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>'; 
					} 
				}
			}
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
     
    
    function addImagesToDb($imageName, $id){
    	   		
    	if($this->checkSession()){
    	
			$images = $this->main_model->getCurrImages($id);
			
			$imageArray = array();
			
			if($images[0]['images'] != null){
			
				$imageArray = json_decode($images[0]['images']);
				array_push($imageArray, $imageName);
			}
			else{
				array_push($imageArray, $imageName);
			}
			
			$this->main_model->updateImages($imageArray, $id);
	    }
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
     
     
    function uploadDegImage($id){
    	   		
    	if($this->checkSession()){
    	
			$valid_formats = array("jpg", "png");
			
			if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
		
				$uploaddir = "upload/".$id."/"; //Image upload directory
			
				if (!file_exists($uploaddir)) {
					mkdir($uploaddir, 0777);
				}
			
				foreach ($_FILES['photos']['name'] as $name => $value){
			
					$filename = stripslashes($_FILES['photos']['name'][$name]);
					$size=filesize($_FILES['photos']['tmp_name'][$name]);

					$image_name = $this->getImageName($filename, $id); 
					
					echo '<div class="preview-box"><img src="'.base_url().$uploaddir.$image_name.'" class="imgList"><a href="'.base_url().'catalog/removeDegImage/'.$image_name.'/'.$id.'" class="img-remove"><img src="'.base_url().'/images/close7.png"></a></div>';
					$newname=$uploaddir.$image_name; 

					//Moving file to uploads folder
					if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname)) { 
				
						$this->addDegImagesToDb($image_name, $id);
					}
					else {
						echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>'; 
					} 
				}
			}
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    function getImageName($filename, $id){
    
    	$ext = explode('.', $filename);
    	   		
    	if($this->checkSession()){
    	
			$images = $this->main_model->getCurrImages($id);
			
			if($images[0]['panorama'] != null){
			
				$imageArray = json_decode($images[0]['panorama']);
				$num = explode('.', $imageArray[count($imageArray)-1]);
			
				$int = (int)$num[0] + 1;
				return $int.".".$ext[1];
			}
			else{
				
				return "1.".$ext[1];
			}
    	}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    function addDegImagesToDb($imageName, $id){
    	   		
    	if($this->checkSession()){
    	
			$images = $this->main_model->getCurrImages($id);
			
			$imageArray = array();
			
			if($images[0]['panorama'] != null){
				$imageArray = json_decode($images[0]['panorama']);
				array_push($imageArray, $imageName);
			}
			else{
				array_push($imageArray, $imageName);
			}
			
		   	$this->main_model->updateDegImages($imageArray, $id);
	   	}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
   
   
    /**description* Function to create a config array related with image upload params
	  * @return Array (Config params)
	  */
	function getConfigArray(){
		   		
    	if($this->checkSession()){
    	
			$config['upload_path'] = 'upload/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '2048';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			return $config;
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
	}
	

    function removeImage($image, $id){
    	
    	if($this->checkSession()){
    	
			$images = $this->main_model->getCurrImages($id);
			
			$imageArray = array();
			
			if($images[0]['images'] != null){
			
				$imageArray = json_decode($images[0]['images']);
				$imageArray = array_diff($imageArray, array($image));
				$location = "/srv/www/magnus.digistore.in/public_html/upload/".$image;
				unlink($location);
			}
			
			$res = $this->main_model->updateImages(array_values($imageArray), $id);
	    	redirect(base_url().'catalog/edit/'.$id, 'refresh');		
	    }
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    function removeDegImage($image, $id){
    	
    	if($this->checkSession()){
    	
			$images = $this->main_model->getCurrImages($id);
			
			$imageArray = array();
			
			if($images[0]['panorama'] != null){
				$imageArray = json_decode($images[0]['panorama']);
				$imageArray = array_diff($imageArray, array($image));
				$location = "/srv/www/magnus.digistore.in/public_html/upload/".$id.'/'.$image;
				unlink($location);
			}
			
			$this->main_model->updateDegImages(array_values($imageArray), $id);
	    	redirect(base_url().'catalog/edit/'.$id, 'refresh');
	    }
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    function update($id){
    	
    	if($this->checkSession()){
    	
			$category = $this->input->post("category");
			
			$attrArray = $this->createAttrArray($_POST);
			$productArray = $this->createProductInfoArray($_POST, $category);
			
			$result = $this->main_model->updateProduct($attrArray, $productArray, $category, $id);
			if($result)
				redirect(base_url().'catalog/edit/'.$id, 'refresh');
    	}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    /**
     *  @desc : This function will check if user is logged in or not
     *  @return : redirects to login page
     */
    function checkSession() {
    
    	$session = $this->session->all_userdata();

        if(!(array_key_exists('loggedIn', $session) && $session['loggedIn']))  {
            return false;
        }
        else{
			$last=$this->session->userdata('lastActivity');
			$sess_out=$this->session->userdata('sess_expiration');
			
			if($sess_out + $last < $this->input->server("REQUEST_TIME")){
				
				$this->session->sess_destroy();
				return false;
			}
			else{
				
				$this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
				return true;
			}
        }
    }
}
