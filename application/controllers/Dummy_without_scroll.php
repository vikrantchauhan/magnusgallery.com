<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dummy_without_scroll extends CI_Controller {

	function __Construct(){
        parent::__Construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('site_model');
    }
     
    function index(){
    	$data['menu'] = $this->getMenuItems();
    	$data["inventory"] = $this->site_model->getInventory();
    	$data['error'] = null;
    	$this->load->view('main_header', $data);
		$this->load->view('new', $data);
    }
    
    function check(){
    	$this->load->view('backlinko');
    }
    
    function getMenuItems(){
    	
    	$data = $this->site_model->getMenu();
    	
    	$main = $data['menu'];
    	$sub = $data['sub'];
    	
    	for($i = 0; $i < count($sub); $i++){
    		
    		$item = $sub[$i];
    		$menuId = $item['m_id'];
    		
    		for($x = 0; $x < count($main); $x++){
    			
    			if($main[$x]['id'] == $menuId){
    				
    				if(!empty($main[$x]['sub']))
    					array_push($main[$x]['sub'], $item);
    				else{
    					$main[$x]['sub'] = array();
    					array_push($main[$x]['sub'], $item);
    				}
    			}
    		} 	
    	}
    	
    	return $main;
    }
    
    function product($id){
    	
    	$id = str_replace('-', ' ', $id);
    	$data['error'] = null;
    	$data['product'] = $this->site_model->getProductDetails($id);
    	$data['menu'] = $this->getMenuItems();
		$this->load->view('product_page_without_scroll', $data);
    }
}

?>
