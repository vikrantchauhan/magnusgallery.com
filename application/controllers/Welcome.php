<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

		function __Construct(){
        parent::__Construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('site_model');
    }
	public function index(){
            $data['menu'] = $this->getMenuItems();
            $data["inventory"] = $this->site_model->getInventory();
            $data['error'] = null;
            $this->load->view('trademark',$data);
	}
        
        function getMenuItems(){
    	
    	$data = $this->site_model->getMenu();
    	
    	$main = $data['menu'];
    	$sub = $data['sub'];
    	
    	for($i = 0; $i < count($sub); $i++){
    		
    		$item = $sub[$i];
    		$menuId = $item['m_id'];
    		
    		for($x = 0; $x < count($main); $x++){
    			
    			if($main[$x]['id'] == $menuId){
    				
    				if(!empty($main[$x]['sub']))
    					array_push($main[$x]['sub'], $item);
    				else{
    					$main[$x]['sub'] = array();
    					array_push($main[$x]['sub'], $item);
    				}
    			}
    		} 	
    	}
    	
    	return $main;
    }
       
}
