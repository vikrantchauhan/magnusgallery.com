<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add extends CI_Controller {


	function __Construct(){
        parent::__Construct();
        $this->load->model('main_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('ajaximageuploader');
    }
    
    
	function menu(){
		
		
		$menu = array();
		$subMenu = array();
		$count = 1;
		
		foreach ($_POST as $key =>$value){
			
			$value = explode('-', $value);
			
			if(count($value) > 1){
			
				$cid = $value[0];
				$pid = $value[1];
				$data = $this->main_model->getProductName($pid);
				
				$item = array( $data['name'], $count, $cid, $pid);
				array_push($subMenu, $item);
			}
			else{
				
				if(is_numeric($value[0])){
				
					$id = $value[0];
					$data = $this->main_model->getCategoryName($id);
					
					$item = array($data['name'], $count, $id);
					array_push($menu, $item);
				}
				else{
					
					$name = $value[0];
					
					$item = array($name, $count, 0);			
					array_push($menu, $item);
				}
			}
			$count++;
		}
		
		$result = $this->main_model->addMenu($menu, $subMenu);
		
		if($result)
			redirect(base_url().'mainpage/menu', 'refresh');
	}
	
        
    function category(){
    	   		
    	if($this->checkSession()){
    		
    		if(isset($_POST) && $_POST['name'] != null){
    			$result = $this->main_model->addCategory($_POST['name']);
    			
    			if($result){
					redirect(base_url().'mainpage/category', 'refresh');
				}
				else{
					echo "Some Error Occured. Please Try Again!";
				}
    		}
    	}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
        
    
    function attribute(){
		   		
    	if($this->checkSession()){
    	
			$array = array();
			
			if(isset($_POST)){
		
				$array = $this->createAttributeArray($_POST);
			}
		
			$result = $this->main_model->addAttribute($array);
			
			if($result){
				redirect(base_url().'mainpage/attribute', 'refresh');
			}
			else{
				echo "Some Error Occured. Please Try Again!";
			}
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
   	function createAttributeArray($data){
    
		$array = array();
    	$subArray = array();
    	array_push($array, $data['attribute-name']);
			
		if(count($data) > 2 ){
	
			$count = count($data) - 1;
			
			for($i = 1; $i < ($count+1); $i++){
			
				$key = $data['sub'.$i];
				
				if($key != null)
					array_push($subArray, $key);
			}
		}
		else{
			if($data['sub1'] != null)
				array_push($subArray, $data['sub1']);
		}
	
		if(!empty($subArray))
			array_push($array, json_encode($subArray, true));
			
		return $array;
    }
    
    
    /**
     *  @desc : This function will check if user is logged in or not
     *  @return : redirects to login page
     */
    function checkSession() {
    
    	$session = $this->session->all_userdata();

        if(!(array_key_exists('loggedIn', $session) && $session['loggedIn']))  {
            return false;
        }
        else{
			$last=$this->session->userdata('lastActivity');
			$sess_out=$this->session->userdata('sess_expiration');
			
			if($sess_out + $last < $this->input->server("REQUEST_TIME")){
				
				$this->session->sess_destroy();
				return false;
			}
			else{
				
				$this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
				return true;
			}
        }
    }
}
