<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update extends CI_Controller {


	function __Construct(){
        parent::__Construct();
        $this->load->model('main_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('ajaximageuploader');
    }
    
        
    function attribute($id){
    
    	if($this->checkSession()){
    	
			$array = array();
			
			if(isset($_POST)){
				$array = $this->createAttributeArray($_POST);
			}
			
			$result = $this->main_model->updateAttribute($array, $id);
			if($result){
				redirect(base_url().'mainpage/attribute', 'refresh');
			}
			else{
				echo "Some Error Occured. Please Try Again!";
			}
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }    
        
    
    function category($id){
    
    	if($this->checkSession()){
    		
    		if(isset($_POST) && $_POST['name'] != null){
    		
    			$result = $this->main_model->updateCategory($_POST['name'], $id);
    			
    			if($result){
					redirect(base_url().'mainpage/category', 'refresh');
				}
				else{
					echo "Some Error Occured. Please Try Again!";
				}
    		}
    	}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }

    
    function createAttributeArray($data){
    
		$array = array();
    	$subArray = array();
    	array_push($array, $data['attribute-name']);
			
		if(count($data) > 2 ){
	
			$count = count($data) - 1;
			
			for($i = 1; $i < ($count+1); $i++){
			
				$key = $data['sub'.$i];
				
				if($key != null)
					array_push($subArray, $key);
			}
		}
		else{
			if($data['sub1'] != null)
				array_push($subArray, $data['sub1']);
		}
	
		if(!empty($subArray))
			array_push($array, json_encode($subArray, true));
			
		return $array;
    }
    
    
    /**
     *  @desc : This function will check if user is logged in or not
     *  @return : redirects to login page
     */
    function checkSession() {
    
    	$session = $this->session->all_userdata();

        if(!(array_key_exists('loggedIn', $session) && $session['loggedIn']))  {
            return false;
        }
        else{
			$last=$this->session->userdata('lastActivity');
			$sess_out=$this->session->userdata('sess_expiration');
			
			if($sess_out + $last < $this->input->server("REQUEST_TIME")){
				
				$this->session->sess_destroy();
				return false;
			}
			else{
				
				$this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
				return true;
			}
        }
    }
}
