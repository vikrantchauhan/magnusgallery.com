<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit extends CI_Controller {


	function __Construct(){
        parent::__Construct();
        $this->load->model('main_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('ajaximageuploader');
    }
    
    function menu(){
       		
    	if($this->checkSession()){
    	
			$data['list'] = $this->main_model->getCategoryList();
			$data['products'] = $this->main_model->getCategoryProducts();
			$data['error'] = null;
			$data['menu'] = array("main" => 'web', "sub"=>"menu");
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/edit_menu', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
        
    function attribute($id){
    	
    	if($this->checkSession()){
    		
    		$data['data'] = $this->main_model->getAttributeData($id);
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"attribute");
			$data['list'] = $this->main_model->getCategoryList();
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/edit_attribute', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
        
    
    function category($id){
    	
    	if($this->checkSession()){
    	
    		$data['info'] = $this->main_model->getCategoryInfo($id);
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"category");
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/edit_category', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }

    
    /**
     *  @desc : This function will check if user is logged in or not
     *  @return : redirects to login page
     */
    function checkSession() {
    
    	$session = $this->session->all_userdata();

        if(!(array_key_exists('loggedIn', $session) && $session['loggedIn']))  {
            return false;
        }
        else{
			$last=$this->session->userdata('lastActivity');
			$sess_out=$this->session->userdata('sess_expiration');
			
			if($sess_out + $last < $this->input->server("REQUEST_TIME")){
				
				$this->session->sess_destroy();
				return false;
			}
			else{
				
				$this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
				return true;
			}
        }
    }
}
