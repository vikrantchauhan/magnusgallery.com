<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainpage extends CI_Controller {


	function __Construct(){
        parent::__Construct();
        $this->load->model('main_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('ajaximageuploader');
    }
    
    function dashboard(){
    	   		
    	if($this->checkSession()){
    		
    		$data['catalog'] = $this->main_model->getCatalogCount();
			$data['category'] = $this->main_model->getCategoryCount();
			$data['option'] = $this->main_model->getMenuOptionCount();
			$data['attribute'] = $this->main_model->getAttributeCount();
			
			$data['menu'] = array("main" => 'dashboard', "sub"=>null);
			$data['error'] = null;
			
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/dashboard_view', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    function catalog(){
    
    	   		
    	if($this->checkSession()){
    		
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"catalog");
			$data['list'] = $this->main_model->getProductList();
			
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/catalog_view', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    function attribute(){
    	   		
    	if($this->checkSession()){
    		
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"attribute");
			$data['list'] = $this->main_model->getAttributeList();
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/attribute_view', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }

       
    function category(){
    	   		
    	if($this->checkSession()){
    	
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"category");
			$data['list'] = $this->main_model->getCategoryList();
			
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/category_view', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    function menu(){
       		
    	if($this->checkSession()){
    	
			$data['list'] = $this->main_model->getCategoryList();
			$data['products'] = $this->main_model->getCategoryProducts();
			$data['menulist'] = $this->main_model->getMenuList();
			$data['error'] = null;
			$data['menu'] = array("main" => 'web', "sub"=>"menu");
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/menu_view', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    function newAttribute(){
    	   		
    	if($this->checkSession()){
    	
			$data['list'] = $this->main_model->getCategoryList();
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"attribute");
			
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/add_attribute', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    function newCategory(){
    	   		
    	if($this->checkSession()){
    	
			$data['error'] = null;
			$data['menu'] = array("main" => 'product', "sub"=>"category");
			$this->load->view('admin/header');
			$this->load->view('admin/dashboard_header', $data);
			$this->load->view('admin/add_category', $data);
			$this->load->view('admin/footer');
		}
		else{
			echo "<script>window.location.href = '".base_url()."';</script>";
		}
    }
    
    
    /**
     *  @desc : This function will check if user is logged in or not
     *  @return : redirects to login page
     */
    function checkSession() {
    
    	$session = $this->session->all_userdata();

        if(!(array_key_exists('loggedIn', $session) && $session['loggedIn']))  {
            return false;
        }
        else{
			$last=$this->session->userdata('lastActivity');
			$sess_out=$this->session->userdata('sess_expiration');
			
			if($sess_out + $last < $this->input->server("REQUEST_TIME")){
				
				$this->session->sess_destroy();
				return false;
			}
			else{
				
				$this->session->set_userdata('lastActivity', $this->input->server("REQUEST_TIME"));
				return true;
			}
        }
    }
    
    function logout(){
    	
    	$this->session->sess_destroy();
    	echo "<script>window.location.href = '".base_url()."admin';</script>";
    }
    
    function deletePro($id){
    	
    	$result = $this->main_model->deleteProduct($id);
    	if($result){
			redirect(base_url().'mainpage/catalog', 'refresh');
		}
		else{
			echo "Some Error Occured. Please Try Again!";
		}
    }
    
    function deleteCat($id){
    	
    	$result = $this->main_model->deleteCategory($id);
    	if($result){
			redirect(base_url().'mainpage/category', 'refresh');
		}
		else{
			echo "Some Error Occured. Please Try Again!";
		}
    }
    
    function deleteAttr($id){
    	
    	$result = $this->main_model->deleteAttribute($id);
    	if($result){
			redirect(base_url().'mainpage/attribute', 'refresh');
		}
		else{
			echo "Some Error Occured. Please Try Again!";
		}
    }
}

?>
