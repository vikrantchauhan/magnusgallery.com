<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model{

	/**
	 * @desc load both db
	 */
	 function __construct(){
	 
        parent::__construct();
    }
	
	
		
	/**description* Function to check if user provided credentials belongs  
	  * to admin or not
	  * @params String (Email)
	  * @params String (Password)
	  * @return Array (Admin info)
      */
	function isAdmin($email,$password){
	    
		$sql = "SELECT name,id FROM admin WHERE name = ? and password = ?"; 
		$data = $this->db->query($sql, array($email, $password));	 	
		if($data->result_array()) {
		    return $data->result_array();
		}
		
	}
	
	
	/**description* Function to fetch the Count of Items displayed as Menu 
					at main site		
	  * @return Array (Menu Items Count)
	  */
	function getMenuOptionCount(){
	
		$sql ="select count(*) as count from menu where m_id is null";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	
	/**description* Function to fetch the Count of category types defined 
					for the site
	  * @return Array (Category Count)
	  */
	function getCategoryCount(){
	
		$sql ="select count(*) as count from category";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	
	/**description* Function to fetch the Catalog Count stored in the database
	  * @return Array (Catalog Count)
	  */
	function getCatalogCount(){
	
		$sql ="select count(*) as count from products";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	/**description* Function to fetch the Attribute Count stored in the database
	  * @return Array (Attribute Count)
	  */
	function getAttributeCount(){
	
		$sql ="select count(*) as count from attributes";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function getProductList(){
		
		$sql = "select p.id, 
					   p.name, 
					   c.name as category, 
					   p.c_id, 
					   images 
				from products p 
				left join 
				category c 
				on p.c_id = c.id 
				order by p.id;";
				
		$data = $this->db->query($sql);
		return $data->result_array();
	}	
	
	function getCategoryList(){
		
		$sql = "select * from category order by id";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function getAttributeList(){
		
		$sql = "select id,
					   name,
					   sub
					   from attributes
					   order by id;";
					   
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function getAttributeData($id){
		
		$sql = "select id,
					   name,
					   sub
					   from attributes 
					   where id = '$id';";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function getCatAttributes($cat_id){
	
		$sql = "select name, sub from attributes where cat_id = '$cat_id';";
		$data = $this->db->query($sql);
		return $data->result_array();
		
	}
	
	
	function addProduct($attrs, $product, $cId){
		
		if(!empty($product)){
		
			$sql = "insert into products (name, c_id, description) values (?, ?, ?)";
			$data = $this->db->query($sql, array($product['name'], $cId, $product['description']));
			$id = $this->db->insert_id();
			
			foreach($attrs as $key => $value){
			
				$sql = "insert into product_attr (a_id, p_id, attr_value) values (?, ?, ?)";
				$data = $this->db->query($sql, array($key, $id, $value));
				$result = $this->db->affected_rows();
			}
			return $id;
		}
		else
			return false;
	}
	
	
	function updateProduct($attrs, $product, $cId, $id){
		
		if(!empty($product)){
		
			$sql = "update products set name = ?, c_id = ?, description = ? where id = ?";
			$data = $this->db->query($sql, array($product['name'], $cId, $product['description'], $id));
			$row = $this->db->affected_rows();
			
			foreach($attrs as $key => $value){
			
				$check = $this->checkAttribute($id, $key);
				
				if(!empty($check)){
					$sql = "update product_attr set attr_value = ? where a_id = ? and p_id = ?";
					$data = $this->db->query($sql, array($value, $key, $id));
					$result = $this->db->affected_rows();
				}
				else{
					$sql = "insert into product_attr (a_id, p_id, attr_value) values (?, ?, ?)";
					$data = $this->db->query($sql, array($key, $id, $value));
					$result = $this->db->affected_rows();
				}
			}
			return $row;
		}
		else
			return false;
	}
	
	
	function checkAttribute($pid, $aid){
		
		$sql = "select * from product_attr where a_id = '$aid' and p_id = '$pid'";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	
	function getCurrImages($id){
		
		$sql = "select images, panorama from products where id = $id";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function updateImages($images, $id){
	
		$image = (!empty($images)) ? json_encode($images, true) : null;
		
		$sql = "update products set images = ? where id = ?";
		$data = $this->db->query($sql, array($image, $id));
		return $this->db->affected_rows();
	}
	
	function updateDegImages($images, $id){
	
		$image = (!empty($images)) ? json_encode($images, true) : null;
	
		$sql = "update products set panorama = ? where id = ?";
		$data = $this->db->query($sql, array($image, $id));
		return $this->db->affected_rows();
	}
	
	
	function addAttribute($array){
		
		$sql = "";
		
		if(count($array) > 1){
			$sql = "insert into attributes (name, sub) values (?, ?)";
		}
		else{
			$sql = "insert into attributes (name) values (?)";
		}
		$data = $this->db->query($sql, $array);
		return $this->db->affected_rows();
	}
	
	function updateAttribute($array, $id){

		$sql = "";
		
		if(count($array) > 1){
			$sql = "update attributes set name = ?, sub = ? where id = ? ";
			$this->db->query($sql, array($array[0], $array[1], $id));
		}
		else{
			$sql = "update attributes set name = ? where id = ? ";
			$this->db->query($sql, array($array[0], $id));
		}
		
		return $this->db->affected_rows();
	}
	
	function addCategory($name){
		
		$sql = "insert into category (name) values (?)";
		$data = $this->db->query($sql, array($name));
		return $this->db->affected_rows();
	}
	
	
	function updateCategory($name, $id){
		
		$sql = "update category set name = ? where id = ?";
		$data = $this->db->query($sql, array($name, $id));
		return $this->db->affected_rows();
	}
	
	
	function deleteProduct($id){
		
		$sql = "delete from products where id = '$id'";
		$data = $this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	
	function deleteCategory($id){
		
		$sql = "delete from category where id = '$id'";
		$data = $this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	
	function getCategoryInfo($id){
		
		$sql = "select * from category where id = '$id'";
		$data = $this->db->query($sql);
		$array = $data->result_array();
		return $array[0];
	}
	
	
	function deleteAttribute($id){
		
		$sql = "delete from attributes where id = '$id'";
		$data = $this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	function getProductInfo($id){
		
		$sql = "select id, name, description, c_id as category, images, panorama from products where id = '$id';";
		$data = $this->db->query($sql);
		$array = $data->result_array();
		return $array[0];
	}
	
	
	function getProductAttrs($id){
	
		$sql = "select p.id, 
					   a.name, 
					   attr_value as value 
				from product_attr p 
				left join attributes a 
				on a.id = p.a_id 
				where p_id = '$id' 
				order by p.id";
				
		$data = $this->db->query($sql);
		return $data->result_array(); 
	}
	
	function categoryProducts($id){
		
		$sql = "select id, name from products where c_id = '$id'";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function getCategoryProducts(){
		
		$sql = "select id, name, c_id from products where c_id is not null order by id";
		$query = $this->db->query($sql);
		$array  = $query->result_array();

		$data = array();
	
		for($i = 0; $i < count($array); $i++){
			
			$cid = $array[$i]['c_id'];
			$name = $array[$i]['name'];
			$id = $array[$i]['id'];
			
			if(array_key_exists($cid, $data)){
				$sub = array($id => $name);
				
				array_push($data[$cid], $sub) ;
			}
			else{
				$data[$cid] = array(array($id=>$name));
			}
		}
		return $data;
	}
	
	
	function getCategoryName($id){
		
		$sql = "select id, name from category where id = '$id'";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		return $array[0];	
	}
	
	
	function getProductName($id){
		
		$sql = "select id, name from products where id = '$id'";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		return $array[0];	
	}
	
	
	function addMenu($menu, $subMenu){
	
		$sql1 = "SET FOREIGN_KEY_CHECKS = 0;";
		$sql2 = "TRUNCATE menu;";
		$sql3 = "SET FOREIGN_KEY_CHECKS = 1;";
		
		$this->db->query($sql1);
		$this->db->query($sql2);
		$this->db->query($sql3);
		
		for ($i = 0; $i < count($menu); $i++){
			
			$sql = "insert into menu (name, sequence, cat_id) values (?, ?, ?)";
			$query = $this->db->query($sql, $menu[$i]);	
			$this->db->affected_rows();
		}	
		
		for ($i = 0; $i < count($subMenu); $i++){
			
			$val = $subMenu[$i];
			
			$data = $this->getMenuId($val[2]);
			array_push($val, $data['id']);
			
			$sql = "insert into menu (name, sequence, cat_id, product_id, m_id) values (?, ?, ?, ?, ?)";
			$query = $this->db->query($sql, $val);	
			$this->db->affected_rows();
		}	
		
		return true;
	}
	
	function getMenuId($id){
		
		$sql = "select id from menu where cat_id = '$id' order by id";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		return $array[0];	
	}
	
	function getMenuList(){
	
		$sql = "select id, name, cat_id, product_id, m_id from menu order by id;";
		$query = $this->db->query($sql);
		$array = $query->result_array();
		
		$main = array();
		
		for($i = 0; $i < count($array); $i++){
			
			$item = $array[$i];
			
			if($item['cat_id'] == 0){
				$main[$item['id']] = $item;
			}
			else{
				
				if($item['product_id'] == null){
					$main[$item['id']] = $item;
				}
				else{
					$main[$item['m_id']]['sub'][$item['id']] = $item;
				}
			}
		}
		
		return $main;
	}
}

?>
