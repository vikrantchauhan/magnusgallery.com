<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends CI_Model{

	/**
	 * @desc load both db
	 */
	 function __construct(){
	 
        parent::__construct();
    }
	
	
		
	/**description* Function to extract menu
	  * to admin or not
	  * @return Array (Menu)
      */
	function getMenu(){
	    
	    $sql = "select id, name, cat_id, sequence, product_id, m_id from menu where m_id is null order by sequence";
	    $sq = "select id, name, cat_id, sequence, product_id, m_id from menu where m_id is not null order by sequence";
	    $query = $this->db->query($sql);
	    $data = $this->db->query($sq);
	    
	    $array['menu'] = $query->result_array();
	    $array['sub'] = $data->result_array();
	    
	    return $array;
	}
	
	function getInventory(){
		
		$sql = "select id, name, images from products order by id";
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	
	function getProductDetails($id){
		
		$sql = "select p.id, 
					   p.name, 
					   p.c_id,
					   c.name as c_name, 
					   group_concat(a.name) as attributes, 
					   group_concat(a.attr_value) as val,
					   p.description,
					   p.images,
					   p.panorama
				from products p 
				left join (select p.id, p.a_id, a.name, p.p_id, p.attr_value from product_attr p left join attributes a on p.a_id = a.id) as a 
				on p.id = a.p_id 
				left join category c
				on p.c_id = c.id
				where p.name = '$id' ;";
		
		$data = $this->db->query($sql);
		$array = $data->result_array();
		return $array[0];
	}
}

?>
