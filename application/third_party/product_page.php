<!DOCTYPE html>
<html>

<head>
    <title>
        Magnus Motorcycle Collection
    </title>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1">
    <link href="<?php echo base_url(); ?>css/product_site.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/main_site.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/product_site_demo.css" type="text/css" rel="stylesheet">
     <link href="<?php echo base_url(); ?>css/lightbox.css" type="text/css" rel="stylesheet">
</head>
<body>


    
    <header>
        <img src="/img/menu-icon.png" id="menu" onclick="clicker()">
        <div class="hnote">
            <?php echo $product['c_name']; ?> &nbsp; &nbsp; | &nbsp; &nbsp; <?php echo $product['name']; ?>
        </div>
    </header>
    
    <br style="clear: left;"/>
	<div id="displaybox">
		
		<a class="page-scroll" href="#close" onclick="start()"> 
			<img src="../img/cancel30.png" />
		</a>
		
		<?php 
			for($i = 0; $i < count($menu); $i++){
			
				$item = $menu[$i];
				
				if(empty($item['sub'])){
		?>		
				
				<a class="page-scroll box" href="<? echo base_url()?>#<?php echo strtolower(str_replace(' ', '',$item['name'])); ?>" onclick="start()"><?php echo $item['name']?></a>
		
		<?php	
				}
				else{
		?>
					<a class="page-scroll box" href="#" onclick="start()"><?php echo $item['name']?>  <span style="font-size: 0.5em">+ </span></a>
		<?php
					for($x = 0; $x < count($item['sub']); $x++){
						
						$sub = $item['sub'][$x];
		?>
						<div class="d">
							<a href="<?php echo base_url(); ?><?php echo str_replace(' ', '-', $sub['name']); ?>"> <?php echo $sub['name']; ?> </a>
						</div>
		<?php
					}
				}
			}
		?>

	</div>
	
  
    <h2 id="top">
    	<img class="top-logo" src="/img/monogram.png"/><?php echo $product['c_name']; ?>
    </h2>

    <div class="subheading">WALL PANEL, <?php echo $product['name']; ?></div>
	
	<div class="main-view-type">
	<div class="mainview main-img">
		<?php
				
			$images = json_decode($product['images'], true);
		?>
			<img src="<?php echo base_url();?>upload/<?php echo $images[0]; ?>" />

	</div>
	
	<div class="demo main-img">
		<p class="load"><img src="<?php echo base_url();?>images/loading_dots.gif"/></p>
	</div>
	<div class="view-type" style="margin-right: 0 !important; bottom: 1%">
		<a href="#" id="mainview" class="view-change">Normal View</a> <span style="color:#ccb312;">&nbsp;|&nbsp;</span>
		<a href="#" id="demo" class="view-change">360 Degree View</a>
	</div>
	</div>
	
    <div id="headerscroll">
                <a href="#otherviews" class="page-scroll">SCROLL DOWN</a>
    </div>
               <div class="dimg">
    <a href="#otherviews" class="page-scroll"><img src="/img/down-arrow.png"/></a>
    </div>
  

    <div class="dmenu" id="otherviews">

		<div class="views <? print_r((count($images) < 2) ? 'hide': '')?>">OTHER VIEWS <img src="/img/down-arrow-2.png" /></div>
		

		<div class="imagebox <? print_r((count($images) < 2) ? 'hide': '')?>">
			<ul>
				<?php
				
					//$images = json_decode($product['images'], true);
					
					$imgCount = (count($images) > 5) ? 6 : count($images); 
				
					for($i = 1; $i < $imgCount; $i++){
				?>
					<li>
						<a href="<?php echo base_url();?>upload/<?php echo $images[$i]; ?>" data-lightbox="roadtrip"><img src="<?php echo base_url();?>upload/<?php echo $images[$i]; ?>" /></a>
					</li>
				<?php	
					}
				?>
			</ul>
		</div>
        
        <div class="content-div">

			<div class="description" id="description">

				<p class="desc">DESCRIPTION
					<img src="/img/down-arrow-2.png" />
				</p>
		
				<div class="desccontent scrollbar" id="scroll"> 
					<div class="contentdesc"><?php echo $product['description']; ?></div>
				</div>
			</div>
               
			<div class="features" id="features">
			
				<?php 
					
					$attrs = explode(",", $product['attributes']);
					$values = explode(",", $product['val']);
					
					for($i = 0; $i < count($attrs); $i++){
				?>
						<div class="featurecontent"><?php echo $attrs[$i]; ?></div>
						<div class="<?php print(($i== (count($attrs) -1 )) ? 'y' : 'z')?>"><?php echo $values[$i]; ?>
						</div>
				<?php		
					}
				?>

			</div>
        </div>
        </div>
       

        <footer>
            <p>TO  ORDER  OR  FOR  MORE  INFORMATION,  PLEASE  <a href="#"> CONTACT@MAGNUSGALLERY.COM </a></p>
        </footer>
        <div class="last" id="contact">
        <div class="h">
            <img src="/img/contact-us-icon.png" style="vertical-align: middle; padding: 2%;" />Contact<span class="g">us</span>
        </div>
        <div class="lastone">
            <div class="lastone1"><a href="#"> CONTACT@MAGNUSGALLERY.COM </a>
            </div>
            <div class="lastone1"><a href="#"> WWW.MAGNUSGALLERY.COM </a>
            </div>
        </div>
    </div>
    <div class="copyright">
        
        <p>COPYRIGHT &copy 2016 MAGNUS</p>
        
        <p><a href="#top" class="page-scroll"> TAKE ME BACK TO TOP </a>  </p>
    
    
        <p class="trademark">* Trademark Desclaimer &nbsp<a href="<?php echo base_url(); ?>trademark"><u>Click Here</u></a></p>
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
     <script type="text/javascript" src="js/Home.js"></script>
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>js/jquery.js"></script>
  
    <!-- Plugin JavaScript -->
   <script src="<?php echo base_url(); ?>js/jquery.easing.min.js"></script>
   <script src="<?php echo base_url(); ?>js/jquery.360rotate.js"></script>
   
   <?php 
   			$panorama = json_decode($product['panorama'], true);
			echo "<script> var path = ".$product['id']."; var frames='".count($panorama)."' </script>";
            echo "<script> var counts = frames/2 ; var c = Math.floor(counts); </script>";
	?>
    <script type="text/javascript">
    	
    	$(function(){
			
			ThreeSixtyRotate('.demo',{

				imgPath: 'upload/'+path+'/', // Path to your product images.
				imgEx: 'jpg', // All images must have this file extension
				zeroPadding: 0, // Zero Padding (ex:1->0、01->1、001->2)
				imgFirstNum: 1,
				totalFrame: frames,
				startFrame: c,
				clockwise: false, // false = counterclockwise
				rotateSpeed: 1,
				inertia: true,
				autoRotate: false, // auto rotation
				frameRate: 24,
				overStop: true, // Pause on hover when autoRotate is true.
				preload: true,
				showDuration: 300
			});
		});

    
		$(window).on('beforeunload', function() {
		   $(window).scrollTop(0);
		});
    </script>
    <script type="text/javascript">
    	$(document).ready(function(){

    		var width = $( window ).width();
    		var height = $( window ).height();


    		var tmpImg = new Image();
			tmpImg.src="<?php echo base_url();?>upload/<?php echo $images[0]; ?>"; //or  document.images[i].src;
			$(tmpImg).one('load',function(){
  			orgWidth = tmpImg.width;
  			orgHeight = tmpImg.height;
  			
  			var orgW= width*0.8;
  			var orgR = orgWidth/orgHeight;
  			var orgH = orgW/orgR;
  			$('.mainview').css('width', orgW);
    		$('.mainview').css('height', orgH);
			});
    		
    		$('#demo').click(function(){
    			var imgh = "<?php echo $specs; ?>";
    			var imgw = "<?php echo $spec; ?>";
    			var divw = width*0.8;
    			var ratio = imgw/imgh;
    			var divh = divw/ratio;
    			$('.demo').css('width', divw);
    			$('.demo').css('height', divh);
    			
    		});

    	});


    </script>
  
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>js/creative.js"></script>
    <script src="<?php echo base_url(); ?>js/lightbox.js"></script>
    <script type="text/javascript">
    	lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })

    </script>
</body>

</html>

