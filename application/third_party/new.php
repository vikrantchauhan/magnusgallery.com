 <div class="topcenter" id="top">
            <div class="menuicon">
                <a href="#"><img src="../img/menu-icon.png" onclick="clicker()">
                    <span class="menutext" onclick="clicker()"> Menu </span>
                </a>
            </div>
            <br style="clear: left;/">
            <img src="../img/banner-logo.png" class="centerlogo">
            <div class="centerhead">
                <p class="hding">
                    INSPIRED BY
                </p>
                <p class="subhding">
                    Passion
                </p>
                <p class="hding">
                    DISCOVER OUR
                </p>
                <p class="subhding">
                    Handcrafted Collections
                </p>
            </div>
        </div>
        <div id="headerscroll">
            <a href="#collection" class="page-scroll" id="scroll-down" style="color:#fff;">SCROLL DOWN</a>
            <div id="header-arrow">
            	<img src="../img/bg-arrow.png"/>
            </div>
        </div>
    </div>


    <div class="content container" id="collection">
        <div class="content-header" >
            <p class="ch-first">Vintedge</p>
            <p class="ch-second">MOTORCYCLES & CARS </p>

        </div>
    </div>
<section class="gallery_1">
	<ul id="flexiselDemo3">
		<?php 
						
				for($i = 0; $i < count($inventory); $i++){
					
					$item = $inventory[$i];
					$images = json_decode($item['images'], true);
			?>
				<li>
		            <div class="showimage"><div id="gallery-img" style="background-image:url('<?php echo base_url()?>upload/<?php echo $images[0]; ?>')" class="slides" alt="Chania"></div>
		                <div class="info"> 
		                	<span><?php echo $item['name']; ?></span>	
			               <div> <a href="<?php echo base_url(); ?><?php $pro=rtrim($item['name']);echo str_replace(' ', '-',$pro) ?>">Details </a></div>
		                </div>
		            </div>
		        </li>
			<?php
				}
			?>                                                      
	</ul>    
</section>

 	<section class="content-about" id="content-about">

        <div class="divide">
            <div class="about" id="aboutus">

                <div class="ab">About
                    <br/><div class="m">MAGNUS</div>
                </div>

                <p>
                    Founded by partners in US, India and Philippines, Magnus is a truly global company in terms of vision, creativity and execution. This global nature combined with our local roots place us in a unique position to draw inspiration from our network of diverse designers to deliver exclusive products leveraging craftsmanship refined over generations of knowledge and experience passed down from father to son.
                </p>
                <p>
                    Our first collection - The Vintedge Collection, is inspired by our founder’s passion for antique designs. The initial designs in the collection are centered around classic American Cars and Motorcycles like the Ford Model T and Cadillac Model A. Our vision is to grow this collection and work with our partners to deliver customized vintage designs.
                </p>


            </div>
        </div>
        <div class="divide">
            <div class="team">

                <div class="ab">Our
                    <br/><div class="m">TEAM</div>
                </div>
                <p>
                    We are enthusiasts of all things exclusive, luxurious and high quality from the world over. Our team consists of designers, artisans and connoisseurs passionate about their work with a mission to captivate and charm consumers with our products.
                </p>
            </div>
        </div>
    </section>



    <section class="content-footer" id="content-footer">

        <div class="footer-fi" id="finecraftsmanship">FINE</div>
        <div class="footer-se">Craftsmanship</div>
    

        <div class="seperate">
            <div class="footerfade">
                <span class="fadetext fi">TRULY&nbsp;<span class="fadetext">VINTEDGE<br></span></span>
                <span class="fadetext se">HANDCRAFTED<br></span>
                <span class="fadetext th">TIMELESS<br></span>
                <span class="fadetext fr">CUSTOM&nbsp;<span class="fadetext">MADE<br></span></span>
                <span class="fadetext ff">STUNNING<br></span>
                <span class="fadetext si">FINE<br></span>
            </div>

        </div>


        <div class="seperate">

            <div class="footer-p">
                The Vintedge collection took more than 9 months to come to fruition. The designers spent more than 3 months researching & perfecting the designs and picking the highest quality wood, fiber & aluminum. Our experienced artisans in India and Philippines lovingly built every piece over 100 days using skills and knowledge passed down to them over generations. Our designers and artisans take pride in the work they do and this ensures that every product made is of the highest quality.
            </div>
        </div>
    </section>


    <section class="last" id="contact">
        <div class="h">
            <img src="../img/contact-us-icon.png" style="vertical-align: middle; padding: 1%; " />Contact<span class="g">us</span>
        </div>
        <div class="lastone">
            <div class="lastone1"><a href="#"> CONTACT@MAGNUSGALLERY.COM </a>
            </div>
            <div class="lastone1"><a href="#"> WWW.MAGNUSGALLERY.COM </a>
            </div>
        </div>
    </section>
     <div class="copyright">
        
        <p>COPYRIGHT &copy 2016 MAGNUS</p>
        
        <p><a href="#top" class="page-scroll"> TAKE ME BACK TO TOP </a>  </p>
    
    
        <p class="trademark">* Trademark Desclaimer &nbsp<a href="<?php echo base_url(); ?>trademark"><u>Click Here</u></a></p>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/Home.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>js/jquery.easing.min.js"></script>




    <script type="text/javascript">
        $(document).scroll(function() {
            var y = $(this).scrollTop();
            if (y > 400) {
                $('.headermenu').fadeIn();

            } else {
                $('.headermenu').fadeOut();

            }
        });
       

    </script>
    <script type="text/javascript">
        $(window).on('beforeunload', function() {
            $(window).scrollTop(0);
        });
        
        
		$(window).bind("load resize scroll",function(e) {
			var y = $(window).scrollTop();
		 
			$("#top").filter(function() {
				return $(this).offset().top < (y + $(window).height()) &&
				       $(this).offset().top + $(this).height() > y;
			}).css('background-position', '50% ' + parseInt(y / 2) + 'px');
		});
    </script>

<script type="text/javascript">

$(window).load(function() {
    $("#flexiselDemo1").flexisel();
    $("#flexiselDemo2").flexisel({
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2
            },
            tablet: { 
                changePoint:768,
                visibleItems: 3
            }
        }
    });

    $("#flexiselDemo3").flexisel({
        visibleItems: 3,
        enableResponsiveBreakpoints: true,
        animationSpeed: 500,
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2
            },
            tablet: { 
                changePoint:769,
                visibleItems: 2
            }
        }
    });

    $("#flexiselDemo4").flexisel({
        clone:false
    });
    
});
</script>

<script src="<?php echo base_url(); ?>js/creative.js"></script>    
    
</body>
</html>
