/*****************Attribute Table******************/

CREATE TABLE attributes (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  sub text,
  create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modify_date timestamp NOT NULL default '0000-00-00 00:00:00',
  created_by varchar(100),
  updated_by varchar(100),
  PRIMARY KEY (name),
  UNIQUE KEY id (id)
);



CREATE trigger attrupdate BEFORE UPDATE ON attributes FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP);



/*****************Category Table******************/

CREATE TABLE category (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modify_date timestamp NOT NULL default '0000-00-00 00:00:00',
  created_by varchar(100),
  updated_by varchar(100),
  PRIMARY KEY (name),
  UNIQUE KEY id (id)
);

CREATE trigger catupdate BEFORE UPDATE ON category FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP);



/*****************Menu Table*********************/

CREATE TABLE menu (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  cat_id int(11) NOT NULL,
  sequence int(11) NOT NULL,
  product_id int(11),
  m_id int(11),
  create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modify_date timestamp NOT NULL default '0000-00-00 00:00:00',
  created_by varchar(100),
  updated_by varchar(100),
  PRIMARY KEY (name),
  UNIQUE KEY id (id),
  CONSTRAINT menu_id FOREIGN KEY (m_id) REFERENCES menu (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT p_id FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE trigger menupdate BEFORE UPDATE ON menu FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP);



/*****************Products Table******************/

CREATE TABLE products (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  c_id int(11) NOT NULL,
  description text NOT NULL,
  images text,
  panorama varchar(100),
  create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modify_date timestamp NOT NULL default '0000-00-00 00:00:00',
  created_by varchar(100),
  updated_by varchar(100),
  PRIMARY KEY (name),
  UNIQUE KEY id (id)
);

CREATE trigger proupdate BEFORE UPDATE ON products FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP);



/*****************Product Attributes Table******************/

CREATE TABLE product_attr (
  id int(11) NOT NULL AUTO_INCREMENT,
  a_id int(11) NOT NULL,
  p_id int(11) NOT NULL,
  attr_value text NOT NULL,
  create_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modify_date timestamp NOT NULL default '0000-00-00 00:00:00',
  created_by varchar(100),
  updated_by varchar(100),
  UNIQUE KEY id (id),
  UNIQUE KEY u_id (a_id, p_id),
  CONSTRAINT pro_id FOREIGN KEY (p_id) REFERENCES products (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT attr_id FOREIGN KEY (a_id) REFERENCES attributes (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE trigger provalupdate BEFORE UPDATE ON product_attr FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP);



