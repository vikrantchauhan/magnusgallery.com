-- MySQL dump 10.13  Distrib 5.6.30, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: magnusdb
-- ------------------------------------------------------
-- Server version	5.6.30-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'magnus','OTA1YmE2YjM2Y2MwNzJlZTQwNTVkYzBkZjQzNDVhYWM0MWZhZTkwMzk3YzU1Y2EyZjBhMDM0NTZkOTgwYWM1MA==');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `sub` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
INSERT INTO `attributes` VALUES (4,'Code',NULL,'2016-04-19 09:06:25','0000-00-00 00:00:00',NULL,NULL),(1,'Materials',NULL,'2016-04-19 09:06:01','0000-00-00 00:00:00',NULL,NULL),(3,'Size','[\"Height\",\"Width\",\"Depth\"]','2016-04-19 09:06:19','0000-00-00 00:00:00',NULL,NULL),(2,'Weight',NULL,'2016-04-19 09:06:08','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger attrupdate BEFORE UPDATE ON attributes FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (2,'Vintedge Cars Collection','2016-04-19 09:05:53','0000-00-00 00:00:00',NULL,NULL),(1,'Vintedge Motorcycles Collection','2016-04-19 09:05:48','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger catupdate BEFORE UPDATE ON category FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `m_id` int(11) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`),
  KEY `menu_id` (`m_id`),
  KEY `p_id` (`product_id`),
  CONSTRAINT `menu_id` FOREIGN KEY (`m_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `p_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'About',0,1,NULL,NULL,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(7,'Cadillac Model A 1903',2,4,2,2,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(11,'Cadillac Sixty Special 1941',2,8,8,2,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(5,'Contact',0,13,NULL,NULL,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(10,'Duessenberg Model J Roadster 1930',2,7,7,2,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(4,'Fine Craftsmanship',0,12,NULL,NULL,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(6,'Ford Model T Turnabout 1914',2,3,1,2,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(12,'Harley Davidson 1907',1,10,3,3,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(13,'Harley Davidson 1925',1,11,5,3,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(9,'Lasalle 303 Roadster 1927',2,6,6,2,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(8,'Oldsmobile Limited Touring 1911',2,5,4,2,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(2,'Vintedge Cars Collection',2,2,NULL,NULL,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL),(3,'Vintedge Motorcycles Collection',1,9,NULL,NULL,'2016-04-19 10:35:38','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger menupdate BEFORE UPDATE ON menu FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `product_attr`
--

DROP TABLE IF EXISTS `product_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `attr_value` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `u_id` (`a_id`,`p_id`),
  KEY `pro_id` (`p_id`),
  CONSTRAINT `attr_id` FOREIGN KEY (`a_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pro_id` FOREIGN KEY (`p_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attr`
--

LOCK TABLES `product_attr` WRITE;
/*!40000 ALTER TABLE `product_attr` DISABLE KEYS */;
INSERT INTO `product_attr` VALUES (1,1,1,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 09:08:54','0000-00-00 00:00:00',NULL,NULL),(2,2,1,'Net 61 Pounds<br />\r\n(28Kgs)','2016-04-19 09:08:54','0000-00-00 00:00:00',NULL,NULL),(3,3,1,'Height 78 (200 cm)<br>Width 5 (13 cm)<br>Depth 47 (120 cm)<br>','2016-04-19 09:08:54','0000-00-00 00:00:00',NULL,NULL),(4,4,1,'Wall 1001','2016-04-19 09:08:54','0000-00-00 00:00:00',NULL,NULL),(5,1,2,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 09:23:41','0000-00-00 00:00:00',NULL,NULL),(6,2,2,'Net 59 Pounds (27 Kgs)','2016-04-19 09:23:41','0000-00-00 00:00:00',NULL,NULL),(7,3,2,'Height 47 (120 cm)<br>Width 83 (210 cm)<br>Depth 7 (18 cm)<br>','2016-04-19 09:23:41','0000-00-00 00:00:00',NULL,NULL),(8,4,2,'Wal 1004','2016-04-19 09:23:41','0000-00-00 00:00:00',NULL,NULL),(9,1,3,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 09:38:46','0000-00-00 00:00:00',NULL,NULL),(10,2,3,'Net 56 Pounds (25.5 Kgs)','2016-04-19 09:38:46','0000-00-00 00:00:00',NULL,NULL),(11,3,3,'Height 43 (110 cm)<br>Width 82 (210 cm)<br>Depth 5.5 (14 cm)<br>','2016-04-19 09:38:46','0000-00-00 00:00:00',NULL,NULL),(12,4,3,'Wall 5001','2016-04-19 09:38:46','0000-00-00 00:00:00',NULL,NULL),(13,1,4,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 09:44:04','0000-00-00 00:00:00',NULL,NULL),(14,2,4,'Net 43.5 Pounds (19.75 Kgs)','2016-04-19 09:44:04','0000-00-00 00:00:00',NULL,NULL),(15,3,4,'Height 47 (120 cm)<br>Width 94 (240 cm)<br>Depth 6 (16 cm)<br>','2016-04-19 09:44:04','0000-00-00 00:00:00',NULL,NULL),(16,4,4,'Wall 1006','2016-04-19 09:44:04','0000-00-00 00:00:00',NULL,NULL),(17,1,5,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 09:50:42','0000-00-00 00:00:00',NULL,NULL),(18,2,5,'Net 66 Pounds (30 Kgs)','2016-04-19 09:50:42','0000-00-00 00:00:00',NULL,NULL),(19,3,5,'Height 43 (110 cm)<br>Width 82 (210 cm)<br>Depth 5.5 (14 cm)<br>','2016-04-19 09:50:42','0000-00-00 00:00:00',NULL,NULL),(20,4,5,'Wall 5002','2016-04-19 09:50:42','0000-00-00 00:00:00',NULL,NULL),(21,1,6,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 09:57:54','0000-00-00 00:00:00',NULL,NULL),(22,2,6,'Net 47 Pounds (21.5 Kgs)','2016-04-19 09:57:54','0000-00-00 00:00:00',NULL,NULL),(23,3,6,'Height 47 (120 cm)<br>Width 47 (120 cm)<br>Depth 8 (20 cm)<br>','2016-04-19 09:57:54','0000-00-00 00:00:00',NULL,NULL),(24,4,6,'Wall 1003','2016-04-19 09:57:54','0000-00-00 00:00:00',NULL,NULL),(25,1,7,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 10:02:17','0000-00-00 00:00:00',NULL,NULL),(26,2,7,'Net 47 Pounds (21.5 Kgs)','2016-04-19 10:02:17','0000-00-00 00:00:00',NULL,NULL),(27,3,7,'Height 47 (120 cm)<br>Width 94 (240 cm)<br>Depth 6 (16 cm)<br>','2016-04-19 10:02:17','0000-00-00 00:00:00',NULL,NULL),(28,4,7,'Wall 1002','2016-04-19 10:02:17','0000-00-00 00:00:00',NULL,NULL),(29,1,8,'Aluminum<br />\r\nPlywood<br />\r\nStonecast<br />\r\nWood (Gemalina)','2016-04-19 10:08:23','0000-00-00 00:00:00',NULL,NULL),(30,2,8,'Net 43.5 Pounds (19.75 Kgs)','2016-04-19 10:08:23','0000-00-00 00:00:00',NULL,NULL),(31,3,8,'Height 39 (100 cm)<br>Width 82 (210 cm)<br>Depth 5 (13 cm)<br>','2016-04-19 10:08:23','0000-00-00 00:00:00',NULL,NULL),(32,4,8,'Wall 1005','2016-04-19 10:08:23','0000-00-00 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `product_attr` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger provalupdate BEFORE UPDATE ON product_attr FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `c_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `images` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(100) DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `panorama` text,
  PRIMARY KEY (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (2,'Cadillac Model A 1903',2,'Cadillac\'s  first  production-year  model,  originally  coined  simply  \"Cadillac\"  and  later the  Model  A,  was  a  single-cylinder  automobile  powered  by  the  \"Little  Hercules\" engine  built  by  Leland  &  Faulconer,  the  engineering  firm  of  Cadillac\'s  founder, Henry  Leland.  Debuting  in  January  1903  at  the  New  York  Automobile  Show,  the Model  A  signaled  to  the  burgeoning  American  auto  industry  that  interchangeable parts  were  the  way  of  the  future.  Leland  was  a  master  of  precision, a  perfectionist who demanded stringent machining and refinement of gears and castings. This quest for  perfection  bore  early  fruit  when  the  first  Cadillac  to  be  exported  to  England—a Model  A—was  entered  in  the  RAC\'s  One  Thousand  Mile  Reliability  Trial  of September 1903 and finished first in its price class on reliability scoring. By early 1903, Cadillac had received orders for 2286 cars, a tremendous number. By mid-week  of  the  New  York  auto  show  at  which  it  was  launched,  Cadillac\'s  salesmanager, William E. Metzger, announced that the  firm had \"sold out,\" and no more orders were accepted. These  luxury  vehicles  of  their  day  were  distinguished  by  patent-leather  mudguards and  a  steering  wheel  rather  than  a  tiller.  All  Model  As  were  painted  red,  weighed 1320  1b  (599  kg),  and  sat  atop  a  76-in.  (1930-mm)  wheelbase.  Top  speed  was  a respectable  30—35  mph  (48—56  km/h),  with  the  engine  rated  at  5  hp.  Final  drive <br />\r\nwas   by   chain   to   the   rear   wheels,   lubricated   by   beef   tallow,   as   the   factoryrecommended. ','[\"1461058067C41.jpg\",\"1461058067C42.jpg\",\"1461058067C43.JPG\",\"1461058067C44.JPG\",\"1461058067C45.jpg\",\"1461058067C46.jpg\",\"1461058067C47.JPG\",\"1461058067C48.jpg\",\"1461058067C49.JPG\"]','2016-04-19 09:23:41','2016-04-20 06:42:04',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\"]'),(8,'Cadillac Sixty Special 1941',2,'For   Cadillac   in   1938,   two   events   in   particular   were   newsworthy   affairs:   the development of a  ness engine, and the  arrival of the  outstanding Sixty Special. The Sixty  Special  sat  atop  a  127-in.  (3226-mm)  chassis  and  featured  a  new  frame  and distinctive  styling.  What  made  the  car  special  was  its  all-new,  notch-backed body with  convertible-style  window  styling;  it  was  also  smaller  than  its  recent V-8 predecessors. An alligator hood—the first such hood to be used on a production car, together  with the  one  found on the  LaSalle of that  year—was also part of the  Sixty Special design. A total of 4000 Sixty Specials were produced for 1941. That year\'s model benefited from  an  across-the-line  horsepower  increase,  to  150  Inp  at  3400 rpm,  and  a compression  ratio  of  7.25:1  for  its  346-cu.-in.  V-8.  The  frame  on  all  series  models was  made  40  percent  stiffer,  and  the  offerings  for  1941  included  the Special  as  a five-passenger  Fleetwood  sedan  available  with  a  \"sunshine  roof\"  or  glass  chauffeur division. The models of that year had a maximum speed of 100 mph (161 km/h), and could accelerate from o to 60 mph (97 km/h) in 14 seconds. The Sixty Specials of 1941 were the first to be made available with the optional four-speed  HydraMatic  automatic  transmission.  Other  available  options—most  of  which <br />\r\nwere uncommon at the time—included a radio, fender skirts, driving lights, mirrors, windshield  washer,  back-up  lights,  integrated  headlamps,  and  air  conditioning, known then as \"Weather Conditioning.\"','[\"1461060557C50.JPG\",\"1461060557C51.jpg\",\"1461060557C52.JPG\",\"1461060557C53.jpg\",\"1461060557C54.jpg\",\"1461060557C55.JPG\",\"1461060557C56.JPG\",\"1461060557C57.jpg\",\"1461060557C58.jpg\",\"1461060557C59.jpg\"]','2016-04-19 10:08:23','2016-04-30 11:51:38',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\",\"37.jpg\",\"38.jpg\",\"39.jpg\",\"40.jpg\"]'),(7,'Duessenberg Model J Roadster 1930',2,'Fred S. Duesenberg made his name in powerful engines and racing before E. L. Cord came on the scene in the 1920s, purchasing Indianapolis-based Duesenberg as part of his  growing  empire.  (Cord\'s  businesses  included  Duesenberg,  Cord,  Auburn,  and other transportation concerns.) The Model J was a product of both men, however, and was introduced on December 1, 1928. Fred Duesenberg was a perfectionist engineer, and  he  joined  Cord  in  promising  that  the  new  Model  J  would  be  \"the  world\'s  finest motor car.\" They were certainly on target. The  Model  J,  with  a  price  tag  of  $8500—sans  coachwork;  that  was  $3500  or  more extra—featured  a  ground-shaking  straight-eight  engine,  impressive  at  4  ft  (1.2  m) long.  The  engine  block  itself  was  a  marvel,  with  chrome  and  nickel  studding  the traditional  apple-green  enamel,  all  under  the  shadow  of  a  giant  cylinder  head  with thirty-two  valves  and  twin  overhead  cams.  The  engine  shared  the  rare  use  of aluminum alloy throughout. Compared to other automobiles of the era, which tended to use cast iron, the Model J gleamed with aluminum from stem to stern. Un-supercharged,  the  J  produced  nearly  265  hp  and  recorded  a  maximum  speed  of 116 mph (187 km/h); it was capable of 94 mph (151 km/h) in second gear. In terms <br />\r\nof  power,  the  Model J  was  top  of  the  food  chain,  putting  out  more  than  three  times that of its competitors. Indeed, it was the fastest automobile on the market.','[\"1461060297C1.JPG\",\"1461060297C2.JPG\",\"1461060297C3.jpg\",\"1461060297C4.jpg\",\"1461060297C5.jpg\",\"1461060297C6.JPG\",\"1461060297C7.JPG\",\"1461060297C8.jpg\",\"1461060297C9.jpg\",\"1461060297C10.JPG\",\"1461060297C11.JPG\",\"1461060297C12.JPG\",\"1461060297C13.JPG\",\"1461060297C14.JPG\",\"1461060297C15.JPG\"]','2016-04-19 10:02:17','2016-04-30 11:49:49',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\",\"37.jpg\",\"38.jpg\",\"39.jpg\"]'),(1,'Ford Model T Turnabout 1914',2,'Henry Ford\'s Model T was a  sensation right out of the  gate. Barely a  year after its introduction in late 1908, the Ford company advised its dealers not to \"send in any more orders until advised by this office.\" And demand did not let up. In addition to its low entry-level price, which purposefully allowed almost every working man to own  a  vehicle  of  his  own,  the  \"Tin  Lizzie\" proved both  roadworthy  and  simple  to repair.  The  company  touted,  \"Drive  a  horse  ten  thousand  miles  day  in  and day out—and you\'ll need a new horse. The Ford will need but new tires.\" Ford  addressed  the  growing  demand  for  the  world\'s  bestselling  automobile by developing  the  concept  of  mass  production  in  relation  to  a  moving  assembly  line. The  system  began  in  August  1913  at  Ford\'s  Highland  Park  facility  in  Dearborn, Michigan.   Henry   Ford   realized   that,   in   order   to   build   thousands—not   just hundreds—of automobiles a day, the assembly line needed to move. In 1914, Ford churned  out  more  than  300,000  Model  Ts;  by  comparison,  the  total  production figure for the other American automakers combined was about a hundred thousand fewer.','[\"1461057118C31.JPG\",\"1461057118C32.JPG\",\"1461057118C33.jpg\",\"1461057118C34.jpg\",\"1461057118C35.jpg\",\"1461057118C36.jpg\",\"1461057118C37.jpg\",\"1461057118C38.jpg\",\"1461057118C39.JPG\",\"1461057118C40.jpg\"]','2016-04-19 09:08:54','2016-04-19 12:37:01',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\",\"37.jpg\",\"38.jpg\",\"39.jpg\"]'),(3,'Harley Davidson 1907',1,'Wall Panel Harley Davidson 1907','[\"1461058801H20.JPG\",\"1461058801h21.JPG\",\"1461058801H22.jpg\",\"1461058801H23.JPG\",\"1461058801H24.JPG\",\"1461058801H25.jpg\",\"1461058801H26.jpg\",\"1461058801H27.JPG\",\"1461058801H28.jpg\",\"1461058801H29.JPG\",\"1461058801H30.JPG\",\"1461058801H31.JPG\",\"1461058801H32.jpg\",\"1461058801H33.jpg\",\"1461058801H34.jpg\"]','2016-04-19 09:38:46','2016-04-30 11:25:20',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\",\"37.jpg\"]'),(5,'Harley Davidson 1925',1,'Wall Panel Harley Davidson 1925','[\"1461059583H1.jpg\",\"1461059583H2.jpg\",\"1461059583H3.JPG\",\"1461059583H4.jpg\",\"1461059583H5.jpg\",\"1461059583H6.jpg\",\"1461059583H7.jpg\",\"1461059583H8.JPG\",\"1461059583H9.jpg\",\"1461059583H10.jpg\",\"1461059583H11.JPG\",\"1461059583H12.jpg\",\"1461059583H13.jpg\",\"1461059583H14.jpg\",\"1461059583H15.jpg\",\"1461059583H16.jpg\",\"1461059583H17.jpg\",\"1461059583H18.JPG\",\"1461059583H19.JPG\"]','2016-04-19 09:50:42','2016-04-30 11:34:40',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\",\"37.jpg\",\"38.jpg\"]'),(6,'Lasalle 303 Roadster 1927',2,'For  a  car-crazy  public,  most  Americans  in  the  1920s  did  not  know  what  they  were missing. That is, until 1927, when Cadillac came out with a sporty \"companion\" car called   the   LaSalle.   Custom   bodywork   fulfilled   the   desire   for   expression   and distinction only to a point—and only for those who could afford it. The vast majority of  models  filling  America\'s  streets  were  boxy  and  relatively  nondescript.  That  all changed  when  Fred  Fisher  and  Alfred  Sloan  of  General  Motors  tasked  Californian Harley  Earl  with  designing  a  car  that  would  fill  the  price  gap  between  the  highest priced   Buick   and   the   lowest-priced   Cadillac.   The   resulting   LaSalle,   the   first production car designed by a stylist, achieved mass appeal. Built  by  Cadillac  and  to  Cadillac  standards,  the  LaSalle  soon  emerged  as  a  trend-setting automobile within GM. In addition to a fresh styling direction on the exterior, the  LaSalle  also  had  a  newly  designed  engine.  Based  on  the  standard  Cadillac V-8, the new power-plant had a more economical design, despite its cylinder heads being \"ribbed\" for aesthetics. In the end, it proved superior to the original in every way. ','[\"1461059943C60.JPG\",\"1461059943C61.JPG\",\"1461059943C62.jpg\",\"1461059943C63.JPG\",\"1461059943C64.JPG\",\"1461059943C65.JPG\",\"1461059943C66.jpg\",\"1461059943C67.JPG\",\"1461059943C68.jpg\",\"1461059943C69.JPG\",\"1461059943C70.JPG\"]','2016-04-19 09:57:54','2016-04-30 11:43:45',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\",\"37.jpg\",\"38.jpg\",\"39.jpg\",\"40.jpg\"]'),(4,'Oldsmobile Limited Touring 1911',2,'When Oldsmobile rolled out its Limited in 1910, luxury was clearly on the mind of William  J.  Mead,  the  former  Buick  executive  picked  by  the  head  of  General Motors, William C. Durant, to lead Oldsmobile. In fact, the entire Oldsmobile line went upscale that year, with the Limited at the top of the scale. The firm\'s catalog for  1910  noted  that  \"such  a  car  cannot  be  produced  rapidly,  therefore  a  limited quantity  can  be  built.\"  The  car  was  colossal,  and  it  grew  in  even  greater size  and power in 1911, when factory prices ranged from $5000 to $7000. The  Oldsmobile  Limited  stretched  to  198  in.  (5029  mm),  weighed  5160 1b  (2341 kg),  and  was  even  picked  to  transport  the  country\'s  extra-large  president,  William Howard  Taft—it  was,  perhaps,  one  of  the  only  cars  able  to  do  so,  comfortably—when he  visited Monroe, Michigan, on an official presidential stop. Assisting Taft and  other  Limited  riders  in  boarding  the  grandiose  automobile  were  two  running boards, necessary because of the entry height. At 42 in. (1067 mm) in diameter, the wheels  for  the  Limited  were  huge,  a  feature  that  Olds  advertised  as  providing the ultimate in road handling and feasible maintenance: it was claimed that the big tires would require less changing. <br />\r\nAlthough  only  a  few  more  than  400  Limiteds  were  produced  over three  years,  the model remained in the public eye thanks to creative promotion. A Limited appeared in  artist  William  Harnden  Foster\'s  painting  Setting  the  Pace  (1909),  which depicts the  Olds  outsizing  and  outrunning  New  York  Central\'s  20th  Century  Limited express   passenger   train,   implying   the   car\'s   unmatched   power   on   the   road.Memorable  advertisements  covered  the  walls  of  dealer  showrooms,  including  a special  chromolithographic  version  of  Setting  the  Pace  that also  appeared  in  social clubs and  motoring and athletic facilities. The  Limited became  etched in  unlimitedfashion in the minds of the Brass Era car—buying public.','[\"1461059206C16.JPG\",\"1461059206C17.JPG\",\"1461059206C18.JPG\",\"1461059206C19.jpg\",\"1461059206C20.jpg\",\"1461059206C21.jpg\",\"1461059206C22.jpg\",\"1461059206C23.jpg\",\"1461059206C24.JPG\",\"1461059206C25.jpg\",\"1461059206C26.jpg\",\"1461059206C27.jpg\",\"1461059206C28.jpg\",\"1461059206C29.jpg\",\"1461059206C30.jpg\"]','2016-04-19 09:44:04','2016-04-30 11:23:06',NULL,NULL,'[\"1.jpg\",\"2.jpg\",\"3.jpg\",\"4.jpg\",\"5.jpg\",\"6.jpg\",\"7.jpg\",\"8.jpg\",\"9.jpg\",\"10.jpg\",\"11.jpg\",\"12.jpg\",\"13.jpg\",\"14.jpg\",\"15.jpg\",\"16.jpg\",\"17.jpg\",\"18.jpg\",\"19.jpg\",\"20.jpg\",\"21.jpg\",\"22.jpg\",\"23.jpg\",\"24.jpg\",\"25.jpg\",\"26.jpg\",\"27.jpg\",\"28.jpg\",\"29.jpg\",\"30.jpg\",\"31.jpg\",\"32.jpg\",\"33.jpg\",\"34.jpg\",\"35.jpg\",\"36.jpg\",\"37.jpg\",\"38.jpg\",\"39.jpg\"]');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger proupdate BEFORE UPDATE ON products FOR EACH ROW SET NEW.modify_date = IF(NEW.modify_date<OLD.modify_date, OLD.modify_date, CURRENT_TIMESTAMP) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_uploads`
--

DROP TABLE IF EXISTS `user_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_uploads` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` text,
  `user_id_fk` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_uploads`
--

LOCK TABLES `user_uploads` WRITE;
/*!40000 ALTER TABLE `user_uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_uploads` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-30 17:24:30
