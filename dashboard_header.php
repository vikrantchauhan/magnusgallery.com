<header>
    <div class="logo">
    	<em>Magnus</em>
    </div>
    
    <div class="search">
        <input placeholder="Search">
        <img src="../images/search.png">
    </div>
    
    <div class="admin">
        <a href="#">
        	<img src="../images/email.png" />
        </a>
        <a href="#">
        	<img src="../images/config.png" />
        </a>
        Admin 
    </div>
</header>


<div class="main">

   <div class="side">

		<a href="#container1"> 
			<div class="menu" id="menu1">
				<img src="../images/dashboard-icon.png" />Dashboard
			</div>
		</a>

        <a href="#procat">
        	<div class="menu" id="menu2">
            	<img src="../images/art.png" />Product Management &nbsp &nbsp &nbsp &nbsp<img src="../images/arrow.png" />
        	</div>
        </a>

        <div class="procat" id="procat">

            <a href="#container2">
            	<div class="pro" id="productmenu">Product</div>
            </a>
            
            <a href="#container3">
            	<div class="pro" id="categorymenu">Category</div>
            </a>
        </div>


		<a href="#">
			<div class="menu" id="menu3"> 
				<img src="../images/tool.png" />Website Management &nbsp &nbsp &nbsp <img src="../images/arrow.png" />
			</div>
		</a>

        <div class="procat" id="staticmenu">

            <a href="#">
            	<div class="pro" id="static">Static</div>
            </a>
            
            <a href="#">
            	<div class="pro" id="smenu">Menu</div>
            </a>
        </div>

		<a href="#">
			<div class="menu">
				<img src="../images/commerce.png" />Attribute Management &nbsp &nbsp &nbsp<img src="../images/arrow.png" />
			</div>
		</a>
	</div>
