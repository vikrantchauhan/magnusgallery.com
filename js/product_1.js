
$("#menu2").click(function()
{
        $("#procat").toggle();
 }); 
$("#menu3").click(function()
{
        $("#staticmenu").toggle();
 }); 


$("#addproduct").click(function(){
	$("#addpop").show();
	$("#closeaddpop").click(function(){
		$("#addpop").hide();

	});
});
$("#attributeadd").click(function(){
    $("#attributepop").show();
    $("#closeattributepop").click(function(){
        $("#attributepop").hide();

    });
})

$("#staticadd").click(function(){
	$("#staticpop").show();
	$("#closestaticpop").click(function(){
		$("#staticpop").hide();

	});
});
$("#categoryadd").click(function(){
  $("#categorypop").show();
  $("#closecategorypop").click(function(){
    $("#categorypop").hide();

  });
});


$("#itemadd").mouseenter(function(){
	$("#activeimg").attr("src",""+base_url+"images/arrowactive.png");
});

$("#itemadd").mouseleave(function(){
	$("#activeimg").attr("src",""+base_url+"images/arrows.png");
});


$("#category1").click(function(){

    $("#formmenu").toggle();
    var src = ($(this).attr("src") === ""+base_url+"images/arrowright.png") ? ""+base_url+"images/arrowdown.png" : ""+base_url+"images/arrowright.png";
    $("#arrowchange").attr("src", src);
});



$(function() {
    $( "#sortable" ).sortable({
      revert: true
    });
    $( "sort, .itemtoadd1" ).disableSelection();
});

$(function() {
    $( "#subsortable" ).sortable({
      revert: true
    });
    $( "sort, .itemtoadd2" ).disableSelection();
});



$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
                $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
               

    });

    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});

$('.addattributes').click(function() {

	  $('.remove').show();
	  var num = parseInt($(this).attr('value')) + 1;
	  
	  $('.block:last').before('<div class="block"><input type="text" class="inputblock" name="sub'+num+'"/ /><span class="remove label"><a href="#"><img src="'+base_url+'images/delete.png">Remove</a></span></div>');
	  $(this).attr('value', num);
});

$('.optionBox').on('click', '.remove', function() {
  $(this).parent().remove();
});



/******************/

$(".dropdown dt a").on('click', function() {
  $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function() {
  $(".dropdown dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});

$('.mutliSelect input[type="checkbox"]').on('click', function() {

  var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    title = $(this).val() + ",";

  if ($(this).is(':checked')) {
    var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel').append(html);
    $(".hida").hide();
  } else {
    $('span[title="' + title + '"]').remove();
    var ret = $(".hida");
    $('.dropdown dt a').append(ret);
  }
});


function readImage(file) {
  var reader = new FileReader();
  var image  = new Image();

  reader.readAsDataURL(file);  
  reader.onload = function(_file) {
    image.src = _file.target.result;
    image.onload = function() {
     
      $('.uploadPreview').append('<br/><br/><img src="' + this.src + '"><span class="remove label"><a href="#">Remove</a></span>' + '<br>');
    };
   $('.uploadPreview').on('click', '.remove', function() {
  $(this).parent().remove();
});


    image.onerror= function() {
      alert('Invalid file type: '+ file.type);
    }; 
      
  };

}
$("#choose").change(function (e) {
  if(this.disabled) {
    return alert('File upload not supported!');
  }

  var F = this.files;
  if (F && F[0]) {
    for (var i = 0; i < F.length; i++) {
      readImage(F[i]);
    }
  }
});
$('select.div-toggler').change(function() {
    var target = $(this).data('target');
    $(target).children().addClass('hide');
    var show = $("option:selected", this).data('show');
    $(show).removeClass('hide');
});



$('#photoimg').on('change', function() {

    var A = $("#imageloadstatus");
    var B = $("#imageloadbutton");

    $("#imageform").ajaxForm({
        target: '#preview',
        beforeSubmit: function() {
            A.show();
            B.hide();
        },
        success: function() {
            A.hide();
            B.show();
        },
        error: function() {
            A.hide();
            B.show();
        }
    }).submit();
});

$('#photodeg').on('change', function() {

    var A = $("#loadstatus");
    var B = $("#loadbutton");
    
    var dir = $('#productName').val();

	$("#imagedeg").ajaxForm({
	    target: '#degpreview',
	    beforeSubmit: function() {
	        A.show();
	        B.hide();
	    },
	    success: function() {
	        A.hide();
	        B.show();
	    },
	    error: function() {
	        A.hide();
	        B.show();
	    }
	}).submit();
});


	$(document).on('click', '.cat-btn', function() {
	
		var text = $(this).attr('text');
		var name = $(this).attr('name');
		var id = $(this).attr('id');
		
		var html = '<dialog id="window'+id+'" class="dialog-window"><img class="remove-dialog" src="'+base_url+'images/close.png" style="vertical-align: middle;"><div>';  

		
		var obj = $.parseJSON(text);
		
		for (var i = 0; i < obj.length; i++ ) {
			
			var sub = obj[i];
			
			for(var key in sub){
				
				html += '<br>&nbsp;&nbsp;'+sub[key]+' <input type="button" data="'+id+'" class="sub-btn" name="'+sub[key]+'" id="'+id+'-'+key+'" value="Add" /><br>';
			}
		}
		
		html += '</div></dialog> ';
		$("#sortable").prepend('<label class="itemtoadd1 itemtoadd">'+name+'<input name="'+name+'" class="item" type="text" value="'+id+'" hidden><img value="'+id+'" class="sub-menu" src="'+base_url+'images/arrows.png" style="vertical-align: middle;"><img class="remove-menu" src="'+base_url+'images/close7.png" style="vertical-align: middle;"><div class="sort" id="subsortable'+id+'"></div>'+html+'</label>');
	});
	
	
	$(document).on('click', '.remove-dialog', function() {
		var id = $(this).closest('.dialog-window').attr('id');
		var dialog = document.getElementById(id);  
		dialog.close();
	});
	
	
	
	
	$(document).on('click', '.sub-btn', function(){
		var name = $(this).attr('name');
		var val = $(this).attr('id');
		var html = '<label class="itemtoadd2 itemtoadd">'+name+'<input type="text" name="'+name+'" value="'+val+'" hidden><img src="'+base_url+'images/close.png" style="vertical-align: middle;" class="remove-menu"></label>';
		var id = $(this).attr('data');
		$(this).closest("#sortable").find('#subsortable'+id).append(html);
	});
	

	$(document).on('click', '#activeimg', function() {
	
		var item = $('#newItem').val();
	
		$("#sortable").prepend('<label class="itemtoadd1 itemtoadd">'+item+'<input type="text" name="'+item+'" class="item" value="'+item+'" hidden><img class="remove-menu" src="'+base_url+'images/close7.png" style="vertical-align: middle;"><div class="sort" id="subsortable"></div></label>');
	});


	$(document).on('click', '.sub-menu', function() {
	
		var id = $(this).attr('value');
		var dialog = document.getElementById('window'+id);  
		dialog.show(); 
	});


	$(document).on('click', '.remove-menu', function() {
		 $(this).closest('.itemtoadd').remove();
	});
	
	
	$(document).on('click', '#message_submit', function() {
		 $(this).closest('.itemtoadd').append('<img class="remove-menu" src="'+base_url+'images/close7.png" style="vertical-align: middle;">');
	});
	

	$("#countries").select2();

	$(".js-example-basic-multiple").select2();
	
	
	$(document).on('click', '#show', function() {
		
		var dialog = document.getElementById('window');  
		$("#window").show(); 
	});
	
	$(document).on('click', '#exit', function() {
		
		var dialog = document.getElementById('window');  
		dialog.close();  
	});

